<img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
# Hybrid MHD-Vlasov Models with Pressure Coupling Scheme

Status: completed 06/2021

## Topic

We have successfully managed to integrate PSYDAC into the STRUPHY code. For this, we translated the different matrices which are used in the MHD
part in STRUPHY to support PSYDAC, and most notably, converted the projection matrices. We
also introduced a set of abstract linear operators (similarly to what is available in scipy) which
we used to construct the matrices in STRUPHY without having to assemble them. Finally, we
implemented a simple coupling strategy between the MHD and the PIC part.
In addition, we submitted several patches to PSYDAC which made it possible to execute the
MHD part on multiple MPI processes. Also, we improved some algorithms implemented in the
PSYDAC library, most notably the solver for Kronecker-structure matrices, and the matrix-free
differentiation operator.

## Info

* Project executive(s): David Schneller
* Duration: 4 months
* Context: Internship
* Funding: none 
* Start date: 03/2021
* End date: 06/2021
* Supervisor(s): Yaman Güclü, [Stefan Possanner](https://www.linkedin.com/in/stefan-possanner-52a1a250/)

## Links

* [Report](reports/2021_IntegratePsydac_report.pdf)
* [Psydac](https://github.com/pyccel/psydac)

## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)