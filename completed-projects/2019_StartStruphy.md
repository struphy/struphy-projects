 <img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
 # Structure-preserving hybrid code for Vlasov-MHD equations applied to energetic particles in Tokamaks 

Status: completed 08/2022

## Topic

This thesis presented the newly developed hybrid MHD-kinetic code [STRUPHY](https://gitlab.mpcdf.mpg.de/struphy/struphy) for investigating interactions between MHD waves and energetic particles in fusion plasmas. In the version
presented in this thesis, the code solves 3d linearized, ideal MHD equations, coupled non-linearly
to fully kinetic 6d Vlasov equations via a current-coupling scheme ([see equations here](https://struphy.pages.mpcdf.de/struphy/sections/models.html#struphy.models.hybrid.LinearMHDVlasovCC)). It was verified that STRUPHY is able to reproduce various ideal MHD results in slab, cylindrical and toroidal geometry
and that coupling effects to a kinetic minority species are captured correctly both in the ion
cyclotron frequency regime as well as in the low-frequency regime of large scale MHD modes.
With the significant numerical improvements listed below, it is believed that STRUPHY brings
several new qualities to the already existing arsenal of hybrid codes. This is mainly because of
the following features:

* The newly developed algorithm in Chapter 3 provably conserves energy and ∇ · B = 0
irrespective of metric, grid spacing, chosen spline degree and degree of time splitting.
This is a consequence of the finite element exterior calculus framework and the resulting
skew-symmetry of the matrix in (3.98) which is subjected to a skew-symmetric, energy-
preserving splitting. It is believed that these built-in conservation properties improve
long-time stability, especially in strongly non-linear simulations.
* The use of B-spline basis function of arbitrary degree results in a high-order method which
guarantees accuracy, the implicit nature of the time integration enables large time steps
in the MHD part and the use of full MHD provides the possibility of exploring the whole
range of MHD waves in contrast to reduced MHD.
* The novel numerical strategy for treating computational domains with a unique singular
pole (edge that is mapped onto a single point) suppresses spurious oscillations at the pole
that may propagate to other regions in the domain and pollute the simulation.

In more detail, the new approach for handling polar singularities combines discrete differential
forms with the IGA-based polar spline framework introduced by Toshniwal et al. [80, 82].
The former provides a natural discretization of grad-, curl- and div-operators in curvilinear
coordinates that preserves the de Rham cochain complex on the discrete level. The latter leads to
continuous eigenfunctions on the mapped domain, including the pole. Based on this framework,
new commuting projectors were constructed in Chapter 5 for the extended de Rham diagram
5.5 (blue arrows therein). These projectors were defined via so-called polar degrees of freedom
(DOFs), obtained as linear combinations of existing tensor product DOFs. Several sufficient
conditions on these linear combinations were stated in order to achieve commutativity and,
moreover, explicit, block-wise representations of all needed extraction/reduction matrices were
given. In Chapter 6, the new framework was tested by means of a magnetosonic eigenmode in
cylindrical geometry for analytical comparison and toroidal Alfv´en eigenmode (TAE) calculations
in toroidal geometry; the correct behavior of the eigenfunctions near the pole was demonstrated,
in contrast to the standard tensor product solver usually used in other codes.

Finally, on the physics side, STRUPHY was verified in slab geometry by calculating shear
Alfv´en and slow sound continuous spectra and conducting phase mixing test cases with resulting
continuum damping. Additionally, an instability in the ion cyclotron frequency regime was
successfully compared to a linear, analytical theory developed in Chapter 2 and the non-linear
saturation mechanism was identified by analyzing the motion of resonant particles. Additionally,
the good conservation properties of STRUPHY were demonstrated and it was shown that these
particularly hold in the non-linear phase. Besides this, global Alfv´en eigenmode and internal kink
mode calculations were conducted in cylindrical geometry and all features of continuous shear
Alfv´en spectra in toroidal geometry, in particular gap formation and gap size, were demonstrated.
Moreover, a particular n = −6 TAE case was benchmarked with other codes and it was shown
that the full-f , full-orbit (6d) treatment of EPs in STRUPHY is able to simulate TAE mode
drive leading to instability.

## Info

* Project executive(s): Florian Holderied
* Duration: 3.5 years
* Context: PhD thesis
* Funding: Max Planck PhD 
* Start date: 02/2019
* End date: 08/2022
* Supervisor(s): [Stefan Possanner](https://www.linkedin.com/in/stefan-possanner-52a1a250/), Xin Wang

## Links

* [PhD thesis](https://mediatum.ub.tum.de/doc/1656539/1656539.pdf)
* [Publication 1](https://www.sciencedirect.com/science/article/pii/S0021999121000358)
* [Publication 2](https://www.sciencedirect.com/science/article/pii/S0021999122003916)

## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)

 
