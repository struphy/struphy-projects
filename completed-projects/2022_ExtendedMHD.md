<img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
# Linear extended MHD equations

Status: completed 07/2023

## Topic

Using FEEC, we have developed for STRUPHY a discretization for the linear homogeneous extended MHD equations. This discretization conserves the discrete helicity and keeps
bounded the discrete Hamiltonian. The discretization was shown to produce only
waves that agree with the analytical dispersion relation derived in chapter 3, which (together with the
helicity conservation) strongly suggests the discretization correctly models the physics encompassed
by the linear homogeneous extended MHD equations. Furthermore, bounding the discrete Hamiltonian makes the discretization stable for long-time simulations, but getting to simulate such long times
was proven to be difficult since the pre-conditioned biconjugate gradient stabilized algorithm, used as
iterative solver for the Hall propagator, does not converge for our particular matrix unless a minuscule
time step length is chosen.

## Info

* Project executive(s): Nathan Mateo Marin Jimenez
* Duration: 9 months
* Context: Master thesis
* Funding: none 
* Start date: 11/2022
* End date: 07/2023
* Supervisor(s): [Stefan Possanner](https://www.linkedin.com/in/stefan-possanner-52a1a250/)

## Links

* [Master thesis](reports/2022_ExtendedMHD_master_thesis.pdf)

## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)