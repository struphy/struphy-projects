<img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
# Implementation of Realistic Tokamak and Stellarator Equilibria

Status: completed 05/2022

## Topic

In this work, we are first introduced to various modern concepts in numerical methods and
approaches to plasma modelling (Ch. 1). Of particular relevance, are the use of differential forms
and finite element exterior calculus in discretizing an MHD equilibrium. In Ch. 2, a Python
package [gvec_to_python](https://gitlab.mpcdf.mpg.de/gvec-group/gvec_to_python) is created to interface with MHD equilibria computed by GVEC, for
the use of hybrid kinetic-fluid simulations in STRUPHY. This involves extensive practical use of
differential forms and applications of B-splines. Then a novel proposal to implement polar splines
in 3D is introduced in Ch. 3, the implementation approach laid out in Ch. 4, and numerical
results obtained in Ch. 5 that validate the implementation and identify potential failure modes of
the concept.

The use of polar splines through an intermediate map, termed the F1 approach in this work, is
found to consistently outperform tensor product univariate B-splines in terms of continuity across
the magnetic axis, in 2D maps as well as in 3D (Table 5.1 and 5.4). Its convergence order in 2D
is found to be comparable to tensor product splines (Table 5.2), except when the differential form
is the third components of a 2-form, or a 3-form. However, in these cases even though the tensor
product splines converge one order faster, they will experience a blowup at the pole, while the
F1 polar splines experience only a discontinuity. We also learn of F1 polar splines’ sensitivity to
the smoothness of Jacobian determinant (Table 5.3, Sec. 5.4.1), and have successfully mitigated
that through an experimental smoothness constraint applied to the magnetic axis of a GVEC
simulation.

## Info

* Project executive(s): Tin Kei Cheng
* Duration: 10 months
* Context: Master thesis
* Funding: none 
* Start date: 08/2021
* End date: 05/2022
* Supervisor(s): Florian Hindenlang, [Stefan Possanner](https://www.linkedin.com/in/stefan-possanner-52a1a250/)

## Links

* [Master thesis](reports/2021_GVECequilibria_master_thesis.pdf)
* [gvec_to_python](https://gitlab.mpcdf.mpg.de/gvec-group/gvec_to_python)

## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)