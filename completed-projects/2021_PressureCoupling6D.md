<img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
# Hybrid MHD-Vlasov Models with Pressure Coupling Scheme

Status: completed 03/2022

## Topic

In this thesis, we presented two energy-conserving hybrid MHD-Vlasov systems with the
pressure coupling scheme namely the full Hamiltonian PC and the perpendicular
Hamiltonian PC systems. The former system is defined by adding the adiabatic
equation of state to the Hamiltonian hybrid system in order to have
a closed system. The latter system is newly derived by substituting the definition of the
total momentum with the other definition which only includes the
perpendicular component of the averaged momentum of the energetic particles. In both of
the systems, the Vlasov equation must be modified with the additional force term which
is naturally derived from the Hamiltonian structure of the system. It was shown that the
additional term comes from a fictitious force generated from the velocity shift $v = v_L − U$
and the frame rotation around the axis Ω = −∇ × U. With these systems, we investigated
the resonant interactions between the shear Alfvén wave and energetic particles with the
shifted Maxwellian distribution.

## Info

* Project executive(s): Byung Kyu Na
* Duration: 14 months
* Context: Master thesis
* Funding: none 
* Start date: 02/2021
* End date: 03/2022
* Supervisor(s): [Stefan Possanner](https://www.linkedin.com/in/stefan-possanner-52a1a250/)

## Links

* [Master thesis](reports/2021_PressureCoupling6D_master_thesis.pdf)

## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)