 <img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
 # Adding an Electromagnetic Gyrokinetic Turbulence Model with Adiabatic Electrons in Struphy

Status: open, apply below.

## Topic


The aim of this project is to enhance the [electrostatic gyrokinetic model](https://gitlab.mpcdf.mpg.de/struphy/struphy-projects/-/blob/main/running-projects/2023_ElectrostaticGKadiabatic.md?ref_type=heads) of [Struphy](https://struphy.pages.mpcdf.de/struphy/index.html) to include electromagnetic effects, hence a dynamical magnetic field. This enables the study of MHD modes within the gyrokinetic paradigm, and makes the comparison with [Struphy's MHD-kinetic hybrid models](https://struphy.pages.mpcdf.de/struphy/sections/models.html#fluid-kinetic-hybrid-models) possible. In particular, the goal of this project is to study toroidal Alfvén eigenmodes (TAEs) with the electromagentic gyrokinetic approach, and compare the results with the MHD-kinetic hybrid model (see section 6 of [this thesis](https://mediatum.ub.tum.de/doc/1656539/document.pdf)).

The dynamical magnetic field will be approximated as $\mathbf B_1 = \nabla \times (A_\parallel \mathbf b_0)$, i.e. the perpendicular component with respect to the equilibirum magnetic field direction $\mathbf b_0$ of the vector potential will be set to zero, $\mathbf A_\perp = 0$. This is a common approach in gyrokinetics (e.g. in the [ORB5 code](https://arxiv.org/pdf/1905.01906.pdf)), as it allows to filter out the unwanted, fast compressioanl Alfvén waves. The model described below can be derived from a Lagrangian fiel theory and has thus several conservation properties, most importantly energy consrevation, that we aim to preserve during discretization. 

The project time frame is 3-4 months. This is possible because most of the building blocks of the model already exist in Struphy today, and merely have to be put together in the right manner. [Struphy data structures](https://struphy.pages.mpcdf.de/struphy/tutorials/tutorial_08_data_structures.html#PIC-data-structures) are MPI parallel and will allow large test runs of the implemented model on the super computer infrastructure of the Max Planck Society.

### Model equations

Implementation of the following gyrokinetic model for the unknowns $(f, \phi, A_\parallel)$:

$$
\begin{align}
 &\frac{\partial f}{\partial t} + \left[ v_\parallel \frac{\mathbf{B}^*}{B^*_\parallel} + \frac{\mathbf{E}^* \times \mathbf{b}_0}{B^*_\parallel}\right] \cdot \nabla f + \frac{q}{m} \left[ \frac{\mathbf{B}^*}{B^*_\parallel} \cdot \mathbf{E}^*\right] \cdot \frac{\partial f}{\partial v_\parallel} = 0\,,
 \\[5mm]
 &-\nabla_\perp \cdot \left(\frac{m n_0}{|B_0|^2}\right) \nabla_\perp \phi + e n_0 \left(1 + \frac{e}{k_B T_{e0}} \phi \right) = q \int f B^*_\parallel\,\textnormal d v_\parallel \textnormal d \mu \,,
 \\[5mm]
 &-\frac 1 \mu_0\nabla_\perp \cdot \nabla_\perp A_\parallel = q \int \mathbf u_\textrm{gc} \cdot \mathbf b_0\, f B^*_\parallel\,\textnormal d v_\parallel \textnormal d \mu \,.
\end{align}
$$

Here, $f(\mathbf{x}, v_\parallel, \mu, t)$ is the guiding center distribution, $\phi(\mathbf{x}, t)$ denotes the electrostatic potential, $A_\parallel(\mathbf{x}, t)$ models the magnetic perturbations such that $\mathbf B_1 = \nabla \times (A_\parallel \mathbf b_0)$, $n_0$ and $T_{e0}$ are electron equilibrium density and temperature, respectively. The totoal magnetic field is $\mathbf B = \mathbf B_0 + \mathbf B_1$ and 

$$
\mathbf{E}^* = -\nabla \phi - (\partial_t A_\parallel)\mathbf b_0 -\frac \mu q \nabla |B_0| \,,  \qquad \mathbf{B}^* = \mathbf{B} + \frac m q v_\parallel \nabla \times \mathbf{b}_0 \,,\qquad B^*_\parallel = \mathbf B^* \cdot \mathbf b_0  \,.
$$

Here, $\mathbf B_0$ with direction $\mathbf b_0 = \mathbf B_0 / |B_0|$ stands for a static equilibrium magnetic field. 

### Lagrangian
The Lagrangian of the above system reads

$$
\begin{equation}
\begin{aligned}
L((\mathbf u_\textrm{gc},a_\parallel), f, \phi) &= \int_\Omega \left[ (m v_\parallel \mathbf b_0 + q\mathbf A_0 + q A_\parallel \mathbf b_0 )   \cdot \mathbf u_\textrm{gc} - \frac{m v_\parallel^2}{2} - \mu |B_0| - q \phi  \right] f B^*_\parallel\,\textnormal d v_\parallel \textnormal d \mu\, \textrm d \mathbf x 
\\
&+ \int_\Omega \frac{m}{|B_0|^2} |\nabla_\perp \phi|^2  f_0 B^*_\parallel\,\textnormal d v_\parallel \textnormal d \mu\, \textrm d \mathbf x 
\\
&- e\int_\Omega \phi n_0 \left(1 + \frac{e}{2k_B T_{e0}} \phi \right)\, \textrm d \mathbf x
-\frac{1}{2\mu_0} \int_\Omega |\nabla_\perp A_\parallel|^2\, \textrm d \mathbf x\,,
\end{aligned}
\end{equation}
$$
where $\nabla \times \mathbf A_0 = \mathbf B_0$, the magnetic perturbation is $\mathbf B_1 = \nabla \times (A_\parallel \mathbf b_0)$ and the electric field is given by $\mathbf E = -\nabla \phi - (\partial_t A_\parallel) \mathbf b_0$. 

Moreover, $(\mathbf u_\textrm{gc}, a_\parallel)$ denote the Eulerian phase space velocity, i.e. the vector field in

$$
\begin{align}
 \dot{\mathbf x}(t) &= \mathbf u_\textrm{gc}(t, \mathbf x, v_\parallel)\,,
 \\[2mm]
 \dot{v_\parallel}(t) &= a_\parallel(t, \mathbf x, v_\parallel)\,.
 \end{align}
$$

The term in the second line of (1),

$$
 H_2 := - \int_\Omega \frac{m}{|B_0|^2} |\nabla_\perp \phi|^2 f_0 B^*_\parallel\,\textnormal d v_\parallel \textnormal d \mu\, \textrm d \mathbf x
$$

is a second-order correction coming from the gyrokinetic coordinate transformation.
The function $f^\textrm{vol} := fB^*_\parallel$ is assumed a conserved volume-form transported with the flow of (2)-(3), i.e. 

$$
\begin{equation}
\partial_t f^\textrm{vol} + \nabla \cdot (\mathbf u_\textrm{gc} f^\textrm{vol}) + \frac{\partial}{\partial v_\parallel}(a_\parallel f^\textrm{vol}) = 0\,.
\end{equation}
$$

Moreover, the Jacobian $B^*_\parallel$ of the gyrokinetic coordinate transformation satisfies the Lioville theorem,

$$
\begin{equation}
\partial_t B^*_\parallel + \nabla \cdot (\mathbf u_\textrm{gc} B^*_\parallel) + \frac{\partial}{\partial v_\parallel}(a_\parallel B^*_\parallel) = 0\,,
\end{equation}
$$

which leads to a transport equation for $f$:

$$
\begin{equation}
\partial_t f + \mathbf u_\textrm{gc} \cdot \nabla  f + a_\parallel\frac{\partial f}{\partial v_\parallel} = 0\,.
\end{equation}
$$


Note that the Lagrangian $L$ does not depend on $a_\parallel$.

The action is as usual given by

$$
 I((\mathbf u_\textrm{gc},a_\parallel), f, \phi) = \int_{t_0}^{t_1} L((\mathbf u_\textrm{gc},a_\parallel), f, \phi)\,\mathrm d t
$$

for arbitrary time intervals. The gyrokinetic Poisson equation and Ampère's law follow from the Euler-Lagrange equations, respectively,

$$
\frac{\delta I}{\delta \phi} = 0\,,\qquad \frac{\delta I}{\delta A_\parallel} = 0\,.
$$

The expressions for $(\mathbf u_\textrm{gc}, a_\parallel)$ could be derived from constrained variations as described in eqs. (9)-(10) of the [Vlasov-Poisson project](https://gitlab.mpcdf.mpg.de/struphy/struphy-projects/-/blob/main/running-projects/2024_VlasovPoissonInhomB.md?ref_type=heads). Here, however, we shall take a different approach and derive the equations of motion in Lagrangian coordinates. These are defined as $(\mathbf x_0, v_{\parallel,0})$ in

$$
 (\mathbf x, v_\parallel) = \Phi_t(\mathbf x_0, v_{\parallel,0})\,,
$$

where $\Phi_t$ is the flow of (2)-(3), i.e. the solution of the ODE system with dependence on the initial condition (which are the Lagrangian coordinates).

To be continued ...


## Info

* Project executive(s): open
* Duration: open
* Context: Internship / Master thesis
* Funding: none
* Start date: open
* End date: open
* Supervisor: Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)

## Links



## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)

 
