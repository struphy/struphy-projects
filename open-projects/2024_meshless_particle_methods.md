 <img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
 # Mesh-less methods for conservation laws 

Status: open

## Info

* Project executive(s): [Valentin Carlier](mailto:Valentin.Carlier@ipp.mpg.de), [Sayyed Amin Raiessi](mailto:sayyed.amin.raiessi.toussi@ipp.mpg.de)
* Duration: unknown
* Context: PhD thesis / Post-doc
* Funding: none
* Start date: open
* End date: open
* Supervisor(s): [Stefan Possanner](mailto:spossann@ipp.mpg.de), [Omar Maj](mailto:omar.maj@ipp.mpg.de)
* Additional information:

## Links

* [Struphy doc](https://struphy.pages.mpcdf.de/struphy/index.html)

## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Omar Maj [omar.maj@ipp.mpg.de](mailto:omar.maj@ipp.mpg.de)
* Valentin Carlier [Valentin.Carlier@ipp.mpg.de](mailto:Valentin.Carlier@ipp.mpg.de)
* Sayyed Amin Raiessi[sayyed.amin.raiessi.toussi@ipp.mpg.de](mailto:sayyed.amin.raiessi.toussi@ipp.mpg.de)

## Topic

The aim is to derive mesh-less particle methods for the solution of conservation laws with the following generic Lagrangian:

$$
\begin{equation}
 L(\mathbf u; p^0, \rho^{vol}, \mathbf A^1, \mathbf B^2) = \int_0^T \int_\Omega \ell(\mathbf q, \mathbf u; p^0, \mathbf A^1, \mathbf B^2)\,\rho^{vol}\, \textrm d \mathbf q\, \textrm d t\,.
\end{equation}
$$ 

Here, $[0, T] \subset \mathbb R$ is a time interval, $\Omega \subset \mathbb R^n$ is assumed open with some set of (curvilinear) coordinates $\mathbf q \in \Omega$ and $\mathbf u: [0, T] \times \Omega \to \mathbb R^n$ is the vector field defining the flow $\Phi_t: \Omega \to \Omega,\, \mathbf q_0 \mapsto \mathbf q$,

$$
 \dot \Phi_t = \mathbf u(t, \Phi_t)\,,\qquad \Phi_0 = \mathbf q_0\,.
$$

Moreover, $\rho^{vol}$ denotes a generic volume form transported by the flow,

$$
 \frac{\textrm d}{\textrm d t} \Big[ \rho^{vol}(t, \Phi_t)\, J_t \Big] = 0 \qquad \Longleftrightarrow \qquad \partial _t \rho^{vol} +  \nabla \cdot (\mathbf u \,\rho^{vol}) = 0\,,
$$

where $J_t = |\textrm{det} D\Phi_t|$ is the determinant of the flow Jacobain $(D\Phi_t)_{i,j} = \partial \Phi_{t, i}/ \partial q_{0,j}$, 
and $\ell$ is a sort of "single-particle" Lagrangian mapping into $\mathbb R$, which besides $(\mathbf q, \mathbf u)$ depends on the following generic functions:

* $p^0$ stands for a 0-form transported by the flow,

$$
 \frac{\textrm d}{\textrm d t}\, p^0(t, \Phi_t) = 0 \qquad \Longleftrightarrow \qquad \partial _t p^0 + \mathbf u \cdot \nabla p^0 = 0\,,
$$

* $\mathbf A^1$ is a 1-form transported by the flow,

$$
 \frac{\textrm d}{\textrm d t} \Big[ D\Phi_t^\top \, \mathbf A^1(t, \Phi_t) \Big] = 0 \qquad \Longleftrightarrow \qquad \partial _t \mathbf A^1 + \mathbf u \cdot \nabla \mathbf A^1 + \nabla \mathbf u \cdot \mathbf A^1 = 0\,,
$$

* $\mathbf B^2$ stands for a 2-form transported by the flow,

$$
 \frac{\textrm d}{\textrm d t} \Big[ J_t\, D\Phi_t^{-1} \, \mathbf B^2(t, \Phi_t) \Big] = 0 \qquad \Longleftrightarrow \qquad \partial _t \mathbf B^2 + \mathbf u \cdot \nabla \mathbf B^2 - \mathbf B^2 \cdot \nabla \mathbf u + \mathbf B^2\,\nabla \cdot \mathbf u = 0\,,
$$

Note that if $\nabla \cdot \mathbf B^2 = 0$ is divergence-free, we can use

$$
 \begin{aligned}
 \mathbf u \cdot \nabla \mathbf B^2 - \mathbf B^2 \cdot \nabla \mathbf u + \mathbf B^2\,\nabla \cdot \mathbf u &= \mathbf u \cdot \nabla \mathbf B^2 - \mathbf B^2 \cdot \nabla \mathbf u + \mathbf B^2\,\nabla \cdot \mathbf u - \mathbf u\, \nabla \cdot \mathbf B^2
 \\[2mm]
 &= - \nabla \times (\mathbf u \times \mathbf B)\,.
 \end{aligned}
$$

The basic ansatz for discretizing the Lagrangian (1) is

$$
\begin{equation}
 \rho^{vol}(t, \mathbf q) \approx \rho^{vol}_h(t, \mathbf q) = \frac 1 N \sum_{k=1}^N w_k\,\delta(\mathbf q - \mathbf q_k(t))\,,
\end{equation}
$$

where each "marker" trajectory $\mathbf q_k(t)$ satisifes

$$
 \dot{\mathbf q}_k = \mathbf u(t, \mathbf q_k(t))\,,\qquad \mathbf q_k(0) = \mathbf q_{k,0} \in \Omega\,.
$$

This leads to 

$$
L \approx L_h(\{\mathbf q_k\}_k) = \frac 1N \sum_{k=1}^N w_k \int_0^T \ell(\mathbf q_k, \dot{\mathbf q}_k; p^0_h(\mathbf q_k), \mathbf A^1_h(\mathbf q_k), \mathbf B^2_h(\mathbf q_k))\,\textrm d t\,,
$$

where $p^0_h$, $\mathbf A^1_h$ and $\mathbf B^2_h$ are sufficiently smooth discretizations of $p^0$, $\mathbf A^1$ and $\mathbf B^2$, respectively. By sufficiently smooth we mean at least $C^1(\Omega)$ for all $t \in [0, T]$, but possibly higher regularity depending on the form of $\ell$ (the first deriative should be Lipshitz). The equations of motions are the Euler-Lagrange equatins of $\ell$:

$$
 \frac{\textrm d}{\textrm d t} \frac{\partial \ell}{\partial \dot{\mathbf q}_k} = \frac{\partial \ell}{\partial \mathbf q_k} + \frac{\partial \ell}{\partial p^0_h} \frac{\partial p^0_h}{\partial \mathbf q_k} + \frac{\partial \ell}{\partial \mathbf A^1_h} \cdot \frac{\partial \mathbf A^1_h}{\partial \mathbf q_k} + \frac{\partial \ell}{\partial \mathbf B^2_h} \cdot \frac{\partial \mathbf B^2_h}{\partial \mathbf q_k} \,.
$$


## Isothermal Euler equations

Assume a static magnetic field $\mathbf B_0 = \nabla \times \mathbf A_0$, then we consider

$$
 \ell = \ell(\mathbf q_k, \dot{\mathbf q}_k; \rho^0_h(\mathbf q_k)) = m\frac{|\dot{\mathbf q}_k|^2}{2} + e \mathbf A_0(\mathbf q_k) \cdot \dot{\mathbf q}_k - \kappa \log [\rho^0_h(\mathbf q_k)]\,,
$$

with $\kappa=const.$ The Euler-Lagrange equations are

$$
 m\ddot{\mathbf q}_k + e \dot{\mathbf q}_k \cdot \nabla \mathbf A_0 = e \nabla \mathbf A_0 \cdot \dot{\mathbf q}_k - \frac{\kappa}{\rho^0_h(t, \mathbf q_k)} \frac{\partial \rho^0_h}{\partial \mathbf q_k}\,.
$$

A possible choice for $\rho^0_h(t, \mathbf q)$ that is consistent with equation (2) reads

$$
 \rho^0_h(t, \mathbf q) = \frac{\rho^{vol}_h(t, \mathbf q)}{\sqrt g (\mathbf q)}  \ * \ S_\varepsilon(\mathbf q) \,,
$$

where $*\ S_\varepsilon(\mathbf q)$ means convolution with a smoothing kernel $S_\varepsilon \in C^1(\Omega)$, and $\sqrt g$ is the metric (volume element) of the Manifold $\Omega$, $\textrm d \mathbf x = \sqrt g \,\textrm d \mathbf q$, where $\mathbf x$ denote Cartesian coordinates. The sum over all particles in the ODE can be performed at a cost $\sim N \log{N}$, by sorting the particles into Cartesian boxes somewhat larger than the support of $S_\varepsilon$.

Simulations can be performed in a Cartesian box $\mathcal B = [0, L]^n \supset \Omega$, where $L$ is large enough to enclose $\Omega$. Then $\sqrt g = const$, and the domain boundary $\partial\Omega$ can be defined by a "flux function" $\psi: \mathcal B \to \mathbb R$ such that 

$$
 \partial \Omega = \{\mathbf x \in \mathcal B\ | \ \psi(\mathbf x) = 0\}\,.
$$


## Ideal MHD equations

### Newcomb 1961

This is after Newcomb (1961). We consider the single-particle Lagrangian

$$
 \ell = \ell(\mathbf q_k, \dot{\mathbf q}_k; \rho^0_h, \mathfrak s^0_h, \rho^{vol}_h, \mathbf B^1, \mathbf B^2) = m\frac{|\dot{\mathbf q}_k|^2}{2} - \frac{1}{\sqrt g} \left[ \frac{\kappa(\mathfrak s^0_h, \rho^0_h)\,(\rho^0_h)^{\gamma - 1}}{\gamma - 1} + \frac{1}{2\mu_0}\frac{\mathbf B^2_h \cdot G \cdot  \mathbf B^2_h}{\rho^{vol}_h \sqrt g} \right] \,,
$$

where $\mathfrak s^{vol} = \mathfrak s^0 \sqrt g$ denotes the entropy per unit volume transported by the flow $\Phi_t$, $G: \Omega \to \mathbb R^{3 \times 3}$ denotes for the metric tensor, and $\kappa$ is given by

$$
 \kappa(\mathfrak s^0, \rho^0) = c \exp \left( \frac{(\gamma - 1)m}{k_B} \frac{s^0}{\rho^0} \right)\,.
$$

Assuming $\sqrt g = const.$ the Euler-Lagrange equations are

$$
 m\ddot{\mathbf q}_k = - \frac{1}{\sqrt g} \frac{\partial }{\partial \mathbf q_k} \left[ \frac{\kappa(\mathfrak s^0_h, \rho^0_h)\,(\rho^0_h)^{\gamma - 1}}{\gamma - 1} + \frac{1}{2\mu_0}\frac{\mathbf B^2_h \cdot G \cdot  \mathbf B^2_h}{\rho^{vol}_h \sqrt g} \right]\,.
$$

The fields are represented with smoothed deltas:

$$
\begin{aligned}
 \rho^0_h(\mathbf q) &= \frac{\rho^{vol}_h(\mathbf q)}{\sqrt g (\mathbf q)}  \ * \ S_\varepsilon(\mathbf q) \,,
 \\[3mm]
 \mathfrak s^0_h(\mathbf q) &= \frac 1 N \sum_{k=1}^N \frac{1}{\sqrt g} \frac{\mathfrak s^{vol}(0, \mathbf q_{k0})}{s^{vol}(0, \mathbf q_{k0})}\,\delta(\mathbf q - \mathbf q_k(t)) \ * \ S_\varepsilon(\mathbf q)\,,
 \\[3mm]
 \mathbf B^2_h(\mathbf q)&= \frac 1 N \sum_{k=1}^N \frac{D\Phi_t(\mathbf q_{k0})  \,\mathbf B^2(0, \mathbf q_{k0})}{s^{vol}(0, \mathbf q_{k0})}\,\delta(\mathbf q - \mathbf q_k(t)) \ * \ S_\varepsilon(\mathbf q)\,,
\end{aligned}
$$

The flow Jacobian can be updated with 

$$
 \frac{\textrm d}{\textrm d t} D \Phi_t = \nabla \mathbf u^\top(\Phi_t)\, D\Phi_t\,,\qquad D\Phi_0 = I\,.
$$

In this formulation, it is difficult to determine $\mathbf u(t, \mathbf q_k)$ and its gradient because we have a second-order ODE for $\mathbf q_k$. We shall thus look at the phase space formulation of the above Lagrangian.


### Phase space formulation

The coordinates are now $\mathbf Z = (\mathbf q, \mathbf v) \in \Omega \times \mathbb R^n$ in phase space. The flow shall be denoted by $\Chi_t = (\Phi_t, \Xi_t): \Omega \times \mathbb R^n \to \Omega \times \mathbb R^n$ for all $t$. The flow velocity is denoted by $\mathbf V = (\mathbf u, \mathbf a): [0, T] \times \Omega \times \mathbb R^n \to \mathbb R^{2n}$. Hence,

$$
 \dot{\Chi}_t = \mathbf V(t, \Chi_t)\,,\qquad \Chi_0 = \mathbf Z_0 \,,\qquad \Longleftrightarrow \qquad 
 \begin{aligned}
 \dot{\mathbf q} &= \mathbf u(t, \mathbf q, \mathbf v)\,,\qquad \mathbf q (0) = \mathbf q_0 \,,
 \\[2mm]
 \dot{\mathbf v} &= \mathbf a(t, \mathbf q, \mathbf v)\,,\qquad \mathbf v (0) = \mathbf v_0 \,.
 \end{aligned}
$$

The Lagrangian reads

$$
 \ell = \ell(\mathbf q_k, \dot{\mathbf q}_k, \mathbf v_k, \dot{\mathbf v}_k; \rho^0_h, \mathfrak s^0_h, \rho^{vol}_h, \mathbf B^1, \mathbf B^2) = m \mathbf v_k \cdot \dot{\mathbf q}_k - m \frac{|\mathbf v_k|^2}{2} - \frac{1}{\sqrt g} \left[ \frac{\kappa(\mathfrak s^0_h, \rho^0_h)\,(\rho^0_h)^{\gamma - 1}}{\gamma - 1} + \frac{1}{2\mu_0}\frac{\mathbf B^2_h \cdot G \cdot \mathbf B^2_h}{\rho^{vol}_h \sqrt g} \right] \,,
$$

The corresponding Euler-Lagrange equations are

$$
 \begin{aligned}
  \dot{\mathbf q}_k &= \mathbf v_k\,,
  \\[2mm]
  m\dot{\mathbf v}_k &= - \frac{1}{\sqrt g} \frac{\partial }{\partial \mathbf q_k} \left[ \frac{\kappa(\mathfrak s^0_h, \rho^0_h)\,(\rho^0_h)^{\gamma - 1}}{\gamma - 1} + \frac{1}{2\mu_0}\frac{\mathbf B^2_h \cdot G \cdot  \mathbf B^2_h}{\rho^{vol}_h \sqrt g} \right]\,.
 \end{aligned}
$$

The flow Jacobian is updated with

$$
 \frac{\textrm d}{\textrm d t} D \Chi_t(\mathbf Z_{k0}) = \nabla \mathbf V^\top(\mathbf Z_k(t))\, D\Chi_t (\mathbf Z_{k0}) \,,\qquad D\Chi_0 = I\,,
$$

where

$$
 \nabla \mathbf V^\top(\mathbf Z_k(t)) = \begin{pmatrix}
 \frac{\partial \mathbf u}{\partial \mathbf q} & \frac{\partial \mathbf u}{\partial \mathbf v}
 \\[2mm]
 \frac{\partial \mathbf a}{\partial \mathbf q} & \frac{\partial \mathbf a}{\partial \mathbf v}
 \end{pmatrix} =
 \begin{pmatrix}
 0 & I
 \\[2mm]
 -\frac{1}{m\sqrt g} \frac{\partial^2 }{\partial \mathbf q_k^2} \left[ \frac{\kappa(\mathfrak s^0_h, \rho^0_h)\,(\rho^0_h)^{\gamma - 1}}{\gamma - 1} + \frac{1}{2\mu_0}\frac{\mathbf B^1_h \cdot \mathbf B^2_h}{\rho^{vol}_h} \right] & 0
 \end{pmatrix}\,.
$$

The only block of the flow Jacobian, 

$$
 D\Chi_t = \begin{pmatrix}
 D_q \Phi_t & D_v \Phi_t
 \\[2mm]
 D_q \Xi_t & D_v \Xi_t
 \end{pmatrix} \,,
$$

we need to determine $\mathbf B^2_h$ is $D_q \Phi_t$. It satisfies the system

$$
\begin{aligned}
 \frac{\textrm d}{\textrm d t} D_q \Phi_t(\mathbf Z_{k0}) &= D_q\Xi_t (\mathbf Z_{k0}) \,,\qquad && D_q\Phi_0 = I\,,
 \\[3mm]
 \frac{\textrm d}{\textrm d t} D_q \Xi_t(\mathbf Z_{k0}) &= - \frac{1}{m\sqrt g} \frac{\partial^2 }{\partial \mathbf q_k^2} \left[ \frac{\kappa(\mathfrak s^0_h, \rho^0_h)\,(\rho^0_h)^{\gamma - 1}}{\gamma - 1} + \frac{1}{2\mu_0}\frac{\mathbf B^2_h \cdot G \cdot \mathbf B^2_h}{\rho^{vol}_h \sqrt g} \right]D_q\Phi_t (\mathbf Z_{k0}) \,,\qquad && D_q\Xi_0 = 0\,,
 \end{aligned}
$$

where, in the second equation, $D_q \Phi_t(\mathbf Z_{k0})$ appears also through the magnetic field

$$
 \mathbf B^2_h(\mathbf q)= \frac 1 N \sum_{k=1}^N \frac{D_q\Phi_t(\mathbf Z_{k0})  \,\mathbf B^2(0, \mathbf q_{k0})}{s^{vol}(0, \mathbf q_{k0})}\,\delta(\mathbf q - \mathbf q_k(t)) \ * \ S_\varepsilon(\mathbf q)\,.
$$

The above matrix equations togehter with the equations for $\dot{\mathbf q}$ and $\dot{\mathbf v}$ form a coupled system of $2 n^2 + 2n$ ODE's for each particle.

Wish list:

* conserve $\nabla \cdot B^2_h$ (strongly). For this we need $\nabla \phi^0(t, \Phi_t) = D_q\Phi_t^{-\top} \nabla \phi^0(0, \mathbf q_{k0})$ for any $\phi^0$ transported by the flow.

* conserve the determinant of $D_q \Phi_t$ for incompressible flow.

* Look at helicity.

## Idea of particle discretization

In what way is a particle discretization such as (2) an approximation to the actual conservation law for the continuous $n$-form? Intuitively, if we are dealing with a quantity such as $\rho^{vol}$ that is transported by the flow $\mathbf q_k(t) = \Phi_t(\mathbf q_{k0})$, it is natural to think of a Klimontovich density like

$$
 \rho^{vol}_\textrm{KL}(t, \mathbf q) = \sum_{k=1}^N \rho^{vol}(t, \mathbf q_k(t))\, \delta(\mathbf q - \mathbf q_k(t))\,.
$$

Because $\rho^{vol}$ is transported by the flow, we have

$$
 \rho^{vol}(t, \mathbf q_k(t)) = \frac{\rho^{vol}(0, \mathbf q_{k0})}{J_t}\,.
$$

We immediately see that in case of incompressible flow, $J_t = 1$, the Klimontovich density is defined entirely by the trajecories $\{\mathbf q_k\}_k$ and the initial condition $\rho^{vol}(0, \cdot)$. This is the essence of a particle method. We will show below that weights $w_k$ can be introduced, as in equation (2), in order to derive a particle discretization via Monte-Carlo approximation of integrals (Aydemir 1993). In this way one is able to handle also the case of compressible flows.

Before going into the details of deriving the weights $w_k$, let us write the Klimontovich densities of other $p$-forms:

* for 0-forms we obtain

$$
\begin{aligned}
 p^0_\textrm{KL}(t, \mathbf q) &= \sum_{k=1}^N p^0(t, \mathbf q_k(t))\, \delta(\mathbf q - \mathbf q_k(t))
 \\[2mm]
 &= \sum_{k=1}^N p^0(0, \mathbf q_{k0})\, \delta(\mathbf q - \mathbf q_k(t))\,.
 \end{aligned}
$$

* for 1-forms we obtain

$$
\begin{aligned}
 \mathbf A^1_\textrm{KL}(t, \mathbf q) &= \sum_{k=1}^N \mathbf A^1(t, \mathbf q_k(t))\, \delta(\mathbf q - \mathbf q_k(t))
 \\[2mm]
 &= \sum_{k=1}^N D\Phi_t^{-\top}(\mathbf q_{k0})\,\mathbf A^1(0, \mathbf q_{k0})\, \delta(\mathbf q - \mathbf q_k(t))\,.
 \end{aligned}
$$

* for 2-forms we obtain

$$
\begin{aligned}
 \mathbf B^2_\textrm{KL}(t, \mathbf q) &= \sum_{k=1}^N \mathbf B^2(t, \mathbf q_k(t))\, \delta(\mathbf q - \mathbf q_k(t))
 \\[2mm]
 &= \sum_{k=1}^N \frac{D\Phi_t(\mathbf q_{k0})}{J_t}\,\mathbf B^2(0, \mathbf q_{k0})\, \delta(\mathbf q - \mathbf q_k(t))\,.
 \end{aligned}
$$

We see that even for incompressible flow, $J_t = 1$, for 1-forms and 2-forms it is not enough to know the trajectories and the initial condition; in addition, knowledge about the flow Jacobain $D\Phi_t: \Omega \to \mathbb R^{n\times n}$ is necessary. The flow Jacobian satisfies

$$
 \frac{\textrm d}{\textrm d t} D \Phi_t = \nabla \mathbf u^\top(\Phi_t)\, D\Phi_t\,,\qquad D\Phi_0 = I\,.
$$


### Volume forms

Let us determine the weights $w_k$ in equation (2) such that $\rho^{vol}_h$ is indeed an approximation of $\rho^{vol}$ transported by the flow $\Phi_t$. For this, we need to show that some distance $||\rho^{vol} - \rho^{vol}_h||$ shrinks as the number of markers $N$ grows. In particular, we aim to show

$$
 \frac{\textrm d}{\textrm d t} \int_\Omega (\rho^{vol} - \rho^{vol}_h)\, \phi^0\,\textrm d \mathbf q = O(N^{-1/2}) \qquad \forall \, \phi^0 \in C^\infty(\Omega)\,.
$$

The particle integral reads

$$
\begin{equation}
\begin{aligned}
 \frac{\textrm d}{\textrm d t} \int_\Omega \rho^{vol}_h \, \phi^0\,\textrm d \mathbf q &= \frac{\textrm d}{\textrm d t} \frac 1N \sum_{k=1}^N w_k \phi^0(\mathbf q_k(t))
 \\[2mm]
 &= \frac 1N \sum_{k=1}^N \Big[\dot w_k \phi^0(\mathbf q_k(t)) + w_k \dot{\mathbf q}_k \cdot \nabla \phi^0(\mathbf q_k(t)) \Big]\,.
 \end{aligned}
 \end{equation}
$$

The exact integral reads

$$
\begin{equation}
\begin{aligned}
 \frac{\textrm d}{\textrm d t} \int_\Omega \rho^{vol} \, \phi^0\,\textrm d \mathbf q &= \int_\Omega \partial_t\rho^{vol} \, \phi^0\,\textrm d \mathbf q
 \\[2mm]
 &= -\int_\Omega \nabla \cdot(\mathbf u\, \rho^{vol}) \, \phi^0\,\textrm d \mathbf q
 \\[2mm]
 &= \int_\Omega \mathbf u\, \rho^{vol} \cdot \nabla \phi^0\,\textrm d \mathbf q\,.
 \end{aligned}
 \end{equation}
$$

By a proper choice of $w_k$, the integral (3) can be shown to be a Monte-Carlo approximation of the integral (4). For this we introduce the volume form $s^{vol}(t, \mathbf q)$, a time-dependent probability distribution on $\Omega$, and denote the expectation value w.r.t $s^{vol}$ by $\mathbb E$. Hence, we can rewrite the integral (4) as

$$
\begin{aligned}
 \int_\Omega \mathbf u\, \rho^{vol} \cdot \nabla \phi^0\,\textrm d \mathbf q &= \int_\Omega \mathbf u\, \frac{\rho^{vol}}{s^{vol}} \cdot \nabla \phi^0\,s^{vol}\,\textrm d \mathbf q 
 \\[2mm]
 &= \int_\Omega \mathbf u\, \frac{\rho^0}{s^0} \cdot \nabla \phi^0\,s^{vol}\,\textrm d \mathbf q 
 \\[2mm]
 &= \mathbb E \left[ \mathbf u\, \frac{\rho^0}{s^0} \cdot \nabla \phi^0\right]\,,
\end{aligned}
$$

where we used $\rho^{vol} = \sqrt g \,\rho^0$, the transformation between volume-and 0-forms. Assuming that $\{\mathbf q_k(t)\}_k$ are $N$ random samples of $s^{vol}(t, \mathbf q)$ at time $t$, an unbiased estimator of $\mathbb E$ is given by

$$

\mathbb E \left[ \mathbf u\, \frac{\rho^0}{s^0} \cdot \nabla \phi^0\right] = \frac 1N \sum_{k=1}^N \mathbf u(\mathbf q_k(t))\, \frac{\rho^0(\mathbf q_k(t))}{s^0(\mathbf q_k(t))} \cdot \nabla \phi^0(\mathbf q_k(t)) + O(N^{-1/2})
$$

Equality of this expression with equation (3) for any $\phi^0$ is achieved iff

$$
 \dot{\mathbf q}_k = \mathbf u(t, \mathbf q_k)\,,\qquad w_k = \frac{\rho^0(\mathbf q_k)}{s^0(\mathbf q_k)}\,,\qquad \dot w_k = 0\,.
$$

The last point $\dot w_k = 0$ determines the equation satisfied by $s^0$:  

$$
\begin{aligned}
 &0 = \dot w_k =  \frac{1}{s^0(\mathbf q_k)} \frac{\textrm d}{\textrm d t} \rho^0(\mathbf q_k) - \frac{\rho^0(\mathbf q_k)}{(s^0(\mathbf q_k))^2} \frac{\textrm d}{\textrm d t} s^0(\mathbf q_k)
 \\[4mm]
 \Leftrightarrow \quad & \frac{\textrm d}{\textrm d t} s^0(\mathbf q_k) = \frac{s^0(\mathbf q_k)}{\rho^0(\mathbf q_k)} \frac{\textrm d}{\textrm d t} \rho^0(\mathbf q_k) \,.
 \end{aligned} \,.
$$

In the general (compressible) case, $\rho^0 = \rho^n /\sqrt g$ is not constant along the characteristics (let's assume for the sake of simplicity that $\sqrt g$ is time-independent):

$$
 \begin{aligned}
 &\partial _t \rho^{vol} +  \nabla \cdot (\mathbf u \,\rho^{vol}) = 0
 \\[4mm]
 \Leftrightarrow \quad & \partial _t (\sqrt g\, \rho^0) +  \nabla \cdot (\mathbf u \,\sqrt g\,\rho^0) = 0
 \\[4mm]
 \Leftrightarrow \quad & \partial _t \rho^0 + \mathbf u \cdot \nabla \rho^0  + \rho^0 \frac{1}{\sqrt g} \nabla \cdot (\mathbf u \sqrt g)= 0
 \\[4mm]
 \Leftrightarrow \quad & \frac{\textrm d}{\textrm d t} \rho^0(\mathbf q_k) = - \left[\rho^0 \frac{1}{\sqrt g} \nabla \cdot (\mathbf u \sqrt g) \right](\mathbf q_k) \,.
 \end{aligned}
$$

Hence we obtain

$$
 \frac{\textrm d}{\textrm d t} s^0(\mathbf q_k) = - \left[s^0 \frac{1}{\sqrt g} \nabla \cdot (\mathbf u \sqrt g) \right](\mathbf q_k) \,,
$$

such that $s^{vol} = \sqrt g \,s^0$ satisfies the same equation as $\rho^{vol}$, with possibly different initial condition:

$$
 \dot w_k = 0 \qquad \Longleftrightarrow \qquad \partial _t s^{vol} +  \nabla \cdot (\mathbf u \,s^{vol}) = 0 \,.
$$



### 0-forms

Let's assume that $p^0$ is transported by the flow $\Phi_t$ as described above. Our ansatz for a particle discrtization of 0-forms is

$$
\begin{equation}
 p^0(t, \mathbf q) \approx p^0_h(t, \mathbf q) = \frac 1 N \sum_{k=1}^N \frac{w_k}{\sqrt g (\mathbf q_k)}\,\delta(\mathbf q - \mathbf q_k(t))\,.
\end{equation}
$$

Again, we want to show

$$
 \frac{\textrm d}{\textrm d t} \int_\Omega (p^0 - p^0_h)\, \phi^3\,\textrm d \mathbf q = O(N^{-1/2}) \qquad \forall \, \phi^3 \in C^\infty(\Omega)\,.
$$

The particle integral reads

$$
\begin{equation}
\begin{aligned}
 \frac{\textrm d}{\textrm d t} \int_\Omega p^0_h \, \phi^3\,\textrm d \mathbf q &= \frac{\textrm d}{\textrm d t} \frac 1N \sum_{k=1}^N \frac{w_k}{\sqrt g (\mathbf q_k)} \phi^0(\mathbf q_k(t)) \sqrt g(\mathbf q_k)
 \\[2mm]
 &= \frac 1N \sum_{k=1}^N \Big[\dot w_k \phi^0(\mathbf q_k(t)) + w_k \dot{\mathbf q}_k \cdot \nabla \phi^0(\mathbf q_k(t)) \Big] \,.
 \end{aligned}
 \end{equation}
$$

The exact integral reads

$$
\begin{aligned}
 \frac{\textrm d}{\textrm d t} \int_\Omega p^0 \, \phi^3\,\textrm d \mathbf q &= \int_\Omega \partial_tp^0 \, \phi^3\,\textrm d \mathbf q 
 \\[2mm]
 &= -\int_\Omega \mathbf u \cdot \nabla p^0 \, \phi^3\,\textrm d \mathbf q
 \\[2mm]
 &= \int_\Omega p^0 \nabla \cdot (\mathbf u\, \phi^3)\,\textrm d \mathbf q
 \\[2mm]
 &= \int_\Omega p^0 \nabla \cdot (\mathbf u \sqrt g) \,\phi^0\,\textrm d \mathbf q + \int_\Omega p^0 \mathbf u \sqrt g \cdot \nabla \phi^0 \,\textrm d \mathbf q\,.
 \end{aligned}
$$

We perform the usual Monte-Carlo approximation,

$$
\begin{equation}
\begin{aligned}
 \int_\Omega p^0 \nabla \cdot (\mathbf u \sqrt g) \,\phi^0\,\textrm d \mathbf q &= \int_\Omega \frac{p^0}{s^0} \frac{1}{\sqrt g} \nabla \cdot (\mathbf u \sqrt g) \,\phi^0\,s^{vol}\,\textrm d \mathbf q
 \\[2mm]
 & = \mathbb E \left[ \frac{p^0}{s^0} \frac{1}{\sqrt g} \nabla \cdot (\mathbf u \sqrt g) \,\phi^0\right]
 \\[2mm]
 &\approx \frac 1N \sum_{k=1}^N \frac{p^0(\mathbf q_k)}{s^0(\mathbf q_k)} \left[\frac{1}{\sqrt g} \nabla \cdot (\mathbf u \sqrt g) \right](\mathbf q_k) \,\phi^0 (\mathbf q_k)\,,
 \\[6mm]
 \int_\Omega p^0 \mathbf u \sqrt g \cdot \nabla \phi^0 \,\textrm d \mathbf q &= \int_\Omega \frac{p^0}{s^0} \mathbf u \cdot \nabla \phi^0\,s^{vol} \,\textrm d \mathbf q
 \\[2mm]
 &= \mathbb E \left[ \frac{p^0}{s^0} \mathbf u \cdot \nabla \phi^0 \right]
 \\[2mm]
 &\approx \frac 1N \sum_{k=1}^N \frac{p^0(\mathbf q_k)}{s^0(\mathbf q_k)} \mathbf u (\mathbf q_k) \cdot \nabla \phi^0 (\mathbf q_k)\,,
\end{aligned}
\end{equation}
$$

Equality of (6) and (7) for any $\phi^0$ is achieved iff

$$
 \dot{\mathbf q}_k = \mathbf u(t, \mathbf q_k)\,,\qquad w_k = \frac{p^0(\mathbf q_k)}{s^0(\mathbf q_k)}\,,\qquad \dot w_k = w_k \left[\frac{1}{\sqrt g} \nabla \cdot (\mathbf u \sqrt g) \right](\mathbf q_k)\,.
$$

The evolution of $w_k$ is consistent iff $s^{vol}$ is transported by the flow $\Phi_t$.



### 1-forms

Let's assume that $\mathbf A^1$ is transported by the flow $\Phi_t$ as described above. Our ansatz for a particle discrtization of 1-forms is

$$
\begin{equation}
 \mathbf A^1(t, \mathbf q) \approx \mathbf A^1_h(t, \mathbf q) = \frac 1 N \sum_{k=1}^N \mathbf w_k\,\delta(\mathbf q - \mathbf q_k(t))\,.
\end{equation}
$$

Again, we want to show

$$
 \frac{\textrm d}{\textrm d t} \int_\Omega (\mathbf A^1 - \mathbf A^1_h) \cdot \mathbf B^2\,\textrm d \mathbf q = O(N^{-1/2}) \qquad \forall \, \mathbf B^2 \in (C^\infty(\Omega))^3\,.
$$

The particle integral reads

$$
\begin{equation}
\begin{aligned}
 \frac{\textrm d}{\textrm d t} \int_\Omega \mathbf A^1_h \cdot \mathbf B^2\,\textrm d \mathbf q &= \frac{\textrm d}{\textrm d t} \frac 1N \sum_{k=1}^N \mathbf w_k \cdot \mathbf B^2(\mathbf q_k(t))
 \\[2mm]
 &= \frac 1N \sum_{k=1}^N \Big[\dot{\mathbf w}_k \cdot \mathbf B^2(\mathbf q_k(t)) +  \dot{\mathbf q}_k \cdot \nabla \mathbf B^2(\mathbf q_k(t)) \cdot \mathbf w_k \Big] \,.
 \end{aligned}
 \end{equation}
$$

The exact integral reads

$$
\begin{aligned}
 \frac{\textrm d}{\textrm d t} \int_\Omega \mathbf A^1 \cdot \mathbf B^2\,\textrm d \mathbf q &= \int_\Omega \partial_t \mathbf A^1 \cdot \mathbf B^2\,\textrm d \mathbf q 
 \\[2mm]
 &= -\int_\Omega \mathbf u \cdot \nabla \mathbf A^1 \cdot \mathbf B^2\,\textrm d \mathbf q - \int_\Omega \mathbf B^2 \cdot \nabla \mathbf u \cdot \mathbf A^1 \,\textrm d \mathbf q 
  \\[2mm]
 &= \int_\Omega \mathbf u \cdot \nabla \mathbf B^2 \cdot \mathbf A^1 \,\textrm d \mathbf q 
 + \int_\Omega  (\nabla \cdot \mathbf u) \,\mathbf A^1 \cdot \mathbf B^2\,\textrm d \mathbf q 
 - \int_\Omega \mathbf B^2 \cdot \nabla \mathbf u \cdot \mathbf A^1 \,\textrm d \mathbf q \,.
 \end{aligned}
$$

We perform the usual Monte-Carlo approximation,

$$
\begin{equation}
\begin{aligned}
 \int_\Omega \mathbf u \cdot \nabla \mathbf B^2 \cdot \mathbf A^1 \,\textrm d \mathbf q 
 &\approx \frac 1N \sum_{k=1}^N \mathbf u(\mathbf q_k) \cdot \nabla \mathbf B^2 (\mathbf q_k) \cdot \frac{\mathbf A^1(\mathbf q_k)}{s^{vol}(\mathbf q_k)}\,,
 \\[6mm]
 \int_\Omega  (\nabla \cdot \mathbf u) \,\mathbf A^1 \cdot \mathbf B^2\,\textrm d \mathbf q  
 &\approx \frac 1N \sum_{k=1}^N  (\nabla \cdot \mathbf u )(\mathbf q_k) \, \frac{\mathbf A^1 (\mathbf q_k)}{s^{vol} (\mathbf q_k)} \cdot \mathbf B^2 (\mathbf q_k)\,,
  \\[6mm]
 \int_\Omega \mathbf B^2 \cdot \nabla \mathbf u \cdot \mathbf A^1 \,\textrm d \mathbf q 
 &\approx \frac 1N \sum_{k=1}^N  \mathbf B^2(\mathbf q_k) \cdot \nabla \mathbf u(\mathbf q_k) \cdot \frac{\mathbf A^1(\mathbf q_k)}{s^{vol}(\mathbf q_k)} \,,
\end{aligned}
\end{equation}
$$

Equality of of (9) and (10) for any $\mathbf B^2$ is achieved iff

$$
 \dot{\mathbf q}_k = \mathbf u(t, \mathbf q_k)\,,\qquad \mathbf w_k = \frac{\mathbf A^1(\mathbf q_k)}{s^{vol}(\mathbf q_k)}\,,\qquad \dot w_k =  (\nabla \cdot \mathbf u )(\mathbf q_k) \, \mathbf w_k - \nabla \mathbf u(\mathbf q_k) \cdot \mathbf w_k\,.
$$

The evolution of $w_k$ is consistent if $s^{vol}$ is transported by the flow $\Phi_t$, because then

$$
\begin{aligned}
 \frac{\textrm d}{\textrm d t} \mathbf w_k &= \frac{1}{s^{vol}(\mathbf q_k)}\frac{\textrm d}{\textrm d t} \mathbf A^1(\mathbf q_k) - \frac{\mathbf A^1(\mathbf q_k)}{(s^{vol}(\mathbf q_k))^2} \frac{\textrm d}{\textrm d t} s^{vol}(\mathbf q_k)
 \\[2mm]
 &= - \frac{1}{s^{vol}(\mathbf q_k)} \nabla \mathbf u (\mathbf q_k) \cdot \mathbf A^1(\mathbf q_k) + \frac{\mathbf A^1(\mathbf q_k)}{(s^{vol}(\mathbf q_k))^2} s^{vol}(\mathbf q_k) \,\nabla \cdot \mathbf u (\mathbf q_k)
  \\[4mm]
  &= -  \nabla \mathbf u (\mathbf q_k) \cdot \mathbf w_k + \mathbf w_k \,\nabla \cdot \mathbf u (\mathbf q_k) \,.
 \end{aligned}
$$

Finally, using that $\mathbf A^1$ and $s^{vol}$ are transported with the flow, the weights can be written as

$$
 \mathbf w_k = \frac{J_t\, D\Phi_t^{-\top} \mathbf A^1(0, \mathbf q_{k0})}{s^{vol}(0, \mathbf q_{k0})} \,.
$$



### 2-forms

Let's assume that $\mathbf B^2$ is transported by the flow $\Phi_t$ as described above. Our ansatz for a particle discrtization of 2-forms is

$$
\begin{equation}
 \mathbf B^2(t, \mathbf q) \approx \mathbf B^2_h(t, \mathbf q) = \frac 1 N \sum_{k=1}^N \mathbf w_k\,\delta(\mathbf q - \mathbf q_k(t))\,.
\end{equation}
$$

Again, we want to show

$$
 \frac{\textrm d}{\textrm d t} \int_\Omega (\mathbf B^2 - \mathbf B^2_h) \cdot \mathbf A^1\,\textrm d \mathbf q = O(N^{-1/2}) \qquad \forall \, \mathbf A^1 \in (C^\infty(\Omega))^3\,.
$$

The particle integral reads

$$
\begin{equation}
\begin{aligned}
 \frac{\textrm d}{\textrm d t} \int_\Omega \mathbf B^2_h \cdot \mathbf A^1\,\textrm d \mathbf q &= \frac{\textrm d}{\textrm d t} \frac 1N \sum_{k=1}^N \mathbf w_k \cdot \mathbf A^1(\mathbf q_k(t))
 \\[2mm]
 &= \frac 1N \sum_{k=1}^N \Big[\dot{\mathbf w}_k \cdot \mathbf A^1(\mathbf q_k(t)) +  \dot{\mathbf q}_k \cdot \nabla \mathbf A^1(\mathbf q_k(t)) \cdot \mathbf w_k \Big] \,.
 \end{aligned}
 \end{equation}
$$

The exact integral reads

$$
\begin{aligned}
 \frac{\textrm d}{\textrm d t} \int_\Omega  \mathbf B^2 \cdot \mathbf A^1\,\textrm d \mathbf q &= \int_\Omega \partial_t \mathbf B^2 \cdot \mathbf A^1\,\textrm d \mathbf q 
 \\[2mm]
 &= -\int_\Omega \mathbf u \cdot \nabla \mathbf B^2 \cdot \mathbf A^1 \,\textrm d \mathbf q + \int_\Omega \mathbf B^2 \cdot \nabla \mathbf u \cdot \mathbf A^1 \,\textrm d \mathbf q - \int_\Omega \nabla \cdot \mathbf u\, \mathbf B^2 \cdot \mathbf A^1\,\textrm d \mathbf q 
  \\[2mm]
 &= \int_\Omega \mathbf u \cdot \nabla \mathbf A^1 \cdot \mathbf B^2 \,\textrm d \mathbf q  
 + \int_\Omega \mathbf B^2 \cdot \nabla \mathbf u \cdot \mathbf A^1 \,\textrm d \mathbf q  \,.
 \end{aligned}
$$

We perform the usual Monte-Carlo approximation,

$$
\begin{equation}
\begin{aligned}
 \int_\Omega \mathbf u \cdot \nabla \mathbf A^1 \cdot \mathbf B^2 \,\textrm d \mathbf q 
 &\approx \frac 1N \sum_{k=1}^N \mathbf u(\mathbf q_k) \cdot \nabla \mathbf A^1 (\mathbf q_k) \cdot \frac{\mathbf B^2(\mathbf q_k)}{s^{vol}(\mathbf q_k)}\,,
  \\[6mm]
 \int_\Omega \mathbf B^2 \cdot \nabla \mathbf u \cdot \mathbf A^1 \,\textrm d \mathbf q 
 &\approx \frac 1N \sum_{k=1}^N  \frac{\mathbf B^2(\mathbf q_k)}{s^{vol}(\mathbf q_k)} \cdot \nabla \mathbf u(\mathbf q_k) \cdot \mathbf A^1(\mathbf q_k) \,,
\end{aligned}
\end{equation}
$$

Equality of of (12) and (13) for any $\mathbf B^2$ is achieved iff

$$
 \dot{\mathbf q}_k = \mathbf u(t, \mathbf q_k)\,,\qquad \mathbf w_k = \frac{\mathbf B^2(\mathbf q_k)}{s^{vol}(\mathbf q_k)}\,,\qquad \dot w_k =   \mathbf w_k \cdot \nabla \mathbf u(\mathbf q_k)\,.
$$

The evolution of $w_k$ is consistent if $s^{vol}$ is transported by the flow $\Phi_t$, because then

$$
\begin{aligned}
 \frac{\textrm d}{\textrm d t} \mathbf w_k &= \frac{1}{s^{vol}(\mathbf q_k)}\frac{\textrm d}{\textrm d t} \mathbf B^2(\mathbf q_k) - \frac{\mathbf B^2(\mathbf q_k)}{(s^{vol}(\mathbf q_k))^2} \frac{\textrm d}{\textrm d t} s^{vol}(\mathbf q_k)
 \\[2mm]
 &= \frac{1}{s^{vol}(\mathbf q_k)} \Big[ \mathbf B^2(\mathbf q_k) \cdot \nabla \mathbf u (\mathbf q_k) - \mathbf B^2(\mathbf q_k) \, \nabla \cdot \mathbf u (\mathbf q_k) \Big] + \frac{\mathbf B^2(\mathbf q_k)}{(s^{vol}(\mathbf q_k))^2} s^{vol}(\mathbf q_k) \,\nabla \cdot \mathbf u (\mathbf q_k)
  \\[4mm]
  &= \mathbf w_k \cdot \nabla \mathbf u (\mathbf q_k) \,.
 \end{aligned}
$$

Finally, using that $\mathbf B^2$ and $s^{vol}$ are transported with the flow, the weights can be written as

$$
 \mathbf w_k = \frac{D\Phi_t \,\mathbf B^2(0, \mathbf q_{k0})}{s^{vol}(0, \mathbf q_{k0})} \,.
$$






 
