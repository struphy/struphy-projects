<img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
 # Particle discretization of conservation laws with non-linear flux functions

Status: running since 14.3.2024.

## Topic

We will develop particle-in-cell (PIC) discretizations of equations of the type

$$
\begin{equation}
\partial_t f + \nabla \cdot (\mathbf F(f) f) = 0\,,
\end{equation}
$$ 

where $f(t, \mathbf z) > 0$ with $\mathbf z \in \mathbb R^n$ is a distribution function and $\mathbf F(f)$ is some non-linear flux. Two generic examples are

1. the heat equation with $\mathbf F(f) = - \nabla \log(f)$,
2. for $n=1$, the Burger's equation with $\mathbf F(f) = f/2$,
3. the Fokker-Planck equation with $\mathbf F(f)(\mathbf z) = -(\mathbf z - \mathbf u) - T\nabla \log(f)$.

In a PIC method, $f$ is approximated by

$$
\begin{equation}
 f(t, \mathbf z) \approx f_h(t, \mathbf z) = \frac 1N \sum_{k=1}^N w_k \delta(\mathbf z - \mathbf z_k(t)) \,,
\end{equation}
$$

where $w_k$ are the marker weights and $\mathbf z_k(t)$ are the marker trajectories, to be determined by solving some ODEs. Substituting the ansatz (2) into equation (1),  muliplying by a test function $\varphi(\mathbf z) \in \mathcal D$ and integrating yields

$$
\begin{align}
&\frac{\mathrm d}{\mathrm d t}\int  f_h\, \varphi\,\textrm d \mathbf z - \int \mathbf F^*[f_h] f_h \cdot \nabla \varphi \,\textrm d \mathbf z = 0
\\[2mm]
\Leftrightarrow\quad & \frac 1N \sum_{k=1}^N w_k \frac{\mathrm d}{\mathrm d t} \varphi(\mathbf z_k(t)) - \frac 1N \sum_{k=1}^N w_k \mathbf F^*[f_h](\mathbf z_k(t))  \cdot \nabla \varphi(\mathbf z_k(t)) = 0
\\[2mm]
\Leftrightarrow\quad & \frac 1N \sum_{k=1}^N w_k \left(\frac{\mathrm d}{\mathrm d t} \mathbf z_k(t) - \mathbf F^*[f_h](\mathbf z_k(t)) \right) \cdot \nabla \varphi(\mathbf z_k(t)) = 0 \,.
\end{align}
$$ 

Here, $\mathbf F^*[f_h]$ is an approximation of $\mathbf F(f_h)$ to be defined below; the latter expression is not well-defined because of the PIC ansatz for $f_h$. One can choose $Nn$ linearly independent test functions to obtain the following system of ODEs:

$$
\frac{\mathrm d}{\mathrm d t} \mathbf z_k(t) = \mathbf F^*[f_h](\mathbf z_k(t)) \,.
$$
It therefore remains to find an accurate expression for $\mathbf F^*[f_h]$ such that 

1. $\mathbf F^*[f_h]$ accurately approximates $\mathbf F(f)$ when $f$ is a smooth function

2. $\mathbf F^*[f_h]$ can be computed very efficiently, even in the particle-in-cell framework, where a very large number of particles are considered.

Let us give an example. One first idea could be to regularize $f_h$ by convoluting it with some regularization kernel $\psi$. Such an approximation would lead to a choice of the form:


$$
\begin{align}
(f_h*\psi)(t,\mathbf z)=\frac{1}{N}\sum_{k=1}^N \psi(\mathbf z - \mathbf z_k(t))\,.
\end{align}
$$
A natural choice for $\mathbf F^*[f_h]$ would then be 


$$\begin{align}
\mathbf F^*[f_h](t,\mathbf z):=\mathbf F(f_h*\psi)(t,\mathbf z)=\mathbf F\left(\frac{1}{N}\sum_{k=1}^N \psi(\mathbf z - \mathbf z_k(t))\right)\,.
\end{align}
$$

While such a choice meets the requirement of accuracy, it is a terrible choice in terms of numerical efficiency, as the number $N$ of particles can be very large. 

One way to adress this problem would be to rather project the sum
$$
(f_h*\psi)(t,\mathbf z)=\frac{1}{N}\sum_{k=1}^N \psi(\mathbf z - \mathbf z_k(t))\,,
$$
onto a smaller vector space, in order to make the size of the sum more manageable. In other words, one could approximate further

$$
\frac{1}{N}\sum_{k=1}^N \psi(\mathbf z - \mathbf z_k(t))\approx \sum_{k=1}^{N_0} g_k(t)\,\Lambda_k(\mathbf z)\,,
$$
where the $\{\Lambda_k\}_{k\in\{1,\dots,N_0\}}$ are some linearly independent functions, and $N_0\ll N$ is much smaller. 
One could think of at least two ways of doing this.

1. Interpolation at Greville Points.
Fix a set of $N_0$ points $(\mathbf z_j)_{j=1}^{N_0}$ and find the coefficients $g_i(t)$ such that 
$$
(f_h*\psi)(t,\mathbf z_j)=\sum_{k=1}^{N_0} g_k(t)\,\Lambda_k(\mathbf z_j)\,,\quad\forall j=1,\dots,N_0\,. 
$$
The regularized function $(f_h*\psi)(t,\cdot)$ is then approximated in the following way:
$$
(f_h*\psi)(t,\mathbf z)\approx \sum_{k=1}^{N_0} g_k(t)\,\Lambda_k(\mathbf z)\,.
$$

2. Orthogonal projection

The requirement is here to solve for $\{g_k(t)\}_{k\in\{1,\dots,N_0\}}$ the following system 

$$
\langle (f_h*\psi)(t,\cdot), \Lambda_j \rangle= \sum_{k=1}^{N_0} g_k(t)\,\langle\Lambda_k\,,\Lambda_j\rangle\,,\quad\forall j=1,\dots,N_0\,. 
$$

## Info

* Project executive(s): Etienne Lehman
* Duration: 3 months
* Context: PhD project 
* Funding: none
* Start date: 14.3.2024
* End date: 15.7.2024
* Supervisors: 
    - Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
    - Claudia Negulescu [claudia.negulescu@math.univ-toulouse.fr](mailto:claudia.negulescu@math.univ-toulouse.fr)

## Links

