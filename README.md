# Apply for an open Struphy project <img src="dog-cartoon-struphy.jpg" width="150" align="right"> 

Check the [list of open projects](https://gitlab.mpcdf.mpg.de/struphy/struphy-projects/-/tree/main/open-projects?ref_type=heads) with variable scope (internships, master's theses, PhD programs) around
the open-source Python plasma physics code [Struphy](https://gitlab.mpcdf.mpg.de/struphy/struphy). 

You can also get an overview of [currently running](https://gitlab.mpcdf.mpg.de/struphy/struphy-projects/-/tree/main/running-projects?ref_type=heads) and [completed Struphy projects](https://gitlab.mpcdf.mpg.de/struphy/struphy-projects/-/tree/main/completed-projects).

If you want to join our awesome team, please contact one of

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)
