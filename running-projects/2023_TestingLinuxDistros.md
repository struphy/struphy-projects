 <img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
 # Docker files and extended .gitlab-ci.yml testing for Ubuntu, Fedora, and CentOS

Status: running since 11/2023.

## Topic

The aim is to extend the testing of Struphy installation, compilation (C and Fortran) and models to different versions of Ubuntu, Fedora and CentOS. Additionally, the testing of AlmaLinux and OpenSUSE is also previsioned. This requires additions to `.gitlab-ci.yml` for running the tests on the MPCDF Gitlab runners.

The new test will run in the scheduled pipeline every week-end, with the exceptin of the latest images, which will be tested at every push. 

## Info

* Project executive(s): Richard Vanderburgh
* Duration: ongoing
* Context: open-source community
* Funding: none
* Start date: 11/2023
* End date: open
* Supervisor: [Stefan Possanner](https://www.linkedin.com/in/stefan-possanner-52a1a250/)

## Links

* [Merge request !453](https://gitlab.mpcdf.mpg.de/struphy/struphy/-/merge_requests/453)

## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)

 
