 <img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
 # Study of two-fluid effects in structure-preserving plasma hybrid codes

Status: running since 10/2023.

## Topic

The aim is to find an efficient geometric implementation of the ideal, extended MHD equations in Struphy. These equations differ from the ideal MHD equations through the Hall- and the electron pressure term in Ohm's law. Consequently, the magnetic field is frozen-in to the electron velocity rather than the ion velocity. The electron term in particular destroys the conservation of helicity, which can lead to magnetic reconnection. A [simplified version of the model](https://struphy.pages.mpcdf.de/struphy/sections/models.html#struphy.models.fluid.LinearExtendedMHD) already exists in Struphy. In the course of this project, the following points will be addressed:

1. Develop a good preconditioner for the [shear Alfvén step](https://struphy.pages.mpcdf.de/struphy/sections/propagators.html#struphy.propagators.propagators_fields.ShearAlfvenB1), possibly using an external library like [Hypre](https://computing.llnl.gov/projects/hypre-scalable-linear-solvers-multigrid-methods).
2. Develop a good preconditioner for the Hall term
3. Implement the linearized extended MHD model for arbitrary mappings and verify against the slab dispersion relation
4. Implement the full nonlinear extended MHD model
5. Study the impact of two-fluid effects on toroidal Alfvén eigenmodes.

### Extended MHD dispersion relation in the homogeneous slab

We recapitulate some derivation from the Master thesis for clarity: assume a homgeneous MHD equilibrium $(n_0, \mathbf U_0 = 0, p_0, \mathbf B_0 = B_0 \mathbf e_z)$ in the slab; the linear mode analysis then yields

$$
\begin{align}
 -i \omega m n_0 \tilde{\mathbf U} + i \mathbf k \tilde p &= i\frac{B_0}{\mu_0}(\mathbf k \times \tilde{\mathbf B}) \times \mathbf e_z\,,
 \\
 -i\omega \tilde{\mathbf B} - B_0 i  \mathbf k \times ( \tilde{\mathbf U} \times \mathbf e_z ) \color{red} + i \mathbf k \times \left[\frac{B_0}{q n_0\mu_0} (i\mathbf k \times \tilde{\mathbf B}  ) \times \mathbf e_z \right] \color{default} &= 0\,,
 \\[3mm]
 -i\omega \tilde p + i \gamma p_0 \mathbf k \cdot \tilde{\mathbf U} &= 0\,.
\end{align}
$$

Here, the Hall term has been marked in red for clarity. The equation for the mass density is decoupled and not needed here. Assuming perturbations with variations  $\mathbf k = (0, k_y, k_z)$, we obtain

$$
\begin{gather}
\mathbf k \times \tilde{\mathbf B} = 
\begin{pmatrix}
k_y \tilde B_z - k_z \tilde B_y
\\[1mm]
k_z \tilde B_x
\\[1mm]
-k_y \tilde B_x
\end{pmatrix} \,, \qquad
(\mathbf k \times \tilde{\mathbf B}) \times \mathbf e_z = 
\begin{pmatrix}
k_z \tilde B_x
\\[1mm]
- k_y \tilde B_z + k_z \tilde B_y
\\[1mm]
0
\end{pmatrix} = 
\underbrace{\begin{pmatrix}
k_z & 0 & 0 
\\[1mm]
0 & k_z & -k_y 
\\[1mm]
0 & 0 & 0
\end{pmatrix}}_{=:J} \tilde{\mathbf B} \,,
\\[2mm]
\color{red} \mathbf k \times \left[ (\mathbf k \times \tilde{\mathbf B}) \times \mathbf e_z \right] = 
\begin{pmatrix}
k_y k_z \tilde B_z - k_z^2 \tilde B_y
\\[1mm]
k_z^2 \tilde B_x
\\[1mm]
-k_y k_z \tilde B_x
\end{pmatrix}  \color{default} \,,
\end{gather}
$$

and

$$
\tilde{\mathbf U} \times \mathbf e_z = 
 \begin{pmatrix}
\tilde U_y
\\[1mm]
- \tilde U_x
\\[1mm]
0
\end{pmatrix} \,,\qquad 
\mathbf k \times (\tilde{\mathbf U} \times \mathbf e_z) = 
\begin{pmatrix}
k_z \tilde U_x
\\[1mm]
k_z \tilde U_y
\\[1mm]
-k_y \tilde U_y
\end{pmatrix} =
\underbrace{ 
\begin{pmatrix}
k_z & 0 & 0 
\\[1mm]
0 & k_z & 0 
\\[1mm]
0 & -k_y & 0
\end{pmatrix}}_{=J^\top}
\tilde{\mathbf U} \,.
$$

Equation (2) can be written as

$$
 \tilde{\mathbf B} \color{red} - i \frac{v_A^2}{\Omega_i\omega} \mathbf k \times \left[ (\mathbf k \times \tilde{\mathbf B}  ) \times \mathbf e_z \right] \color{default} = -\frac{B_0}{\omega}  J^\top \tilde{\mathbf U}  \,,
$$

where we introduced the Alfvén velocity and the cyclotron frequency, respectively,

$$
 v_A = \frac{B_0}{\sqrt{\mu_0 m n_0}}\,,\qquad \Omega_i = \frac{q B_0}{m}\,.
$$

In order to obtain $\tilde{\mathbf B}$ explicitely, we need to compute the following inverse:

$$
M^{-1} = 
\begin{pmatrix}
1 & \red{a} & \red{-b} 
\\[1mm] 
\red{-a} & 1 & 0
\\[1mm] 
\red{b} & 0 & 1
\end{pmatrix}^{-1} = 
\frac{1}{1 \red{+ a^2 + b^2}}
\begin{pmatrix}
1 & \red{-a} & \red{b}
\\[1mm] 
\red{a} & 1 \red{+ b^2} & \red{ab}
\\[1mm] 
\red{-b} & \red{ab} & 1 \red{+ a^2} \,.
\end{pmatrix}
$$

with

$$
 a = i A \frac{k_z^2}{\omega}\,,\qquad b = iA \frac{k_yk_z}{\omega}\,,\qquad A = \frac{v_A^2}{\Omega_i}\,.
$$

Hence one obtains

$$
 \tilde{\mathbf B} = -\frac{B_0}{\omega} M^{-1} J^\top \tilde{\mathbf U}  \,.
$$

The pressure is given by

$$
 \tilde p = \frac{\gamma p_0}{\omega}(k_y \tilde U_y + k_z \tilde U_z)\,.
$$

The momentum conservation law (1) can be written as

$$
\begin{align}
 &-i \omega m n_0 \tilde{\mathbf U} + i \begin{pmatrix} 0 \\ k_y \\ k_z \end{pmatrix} \tilde p - i\frac{B_0}{\mu_0} J \tilde{\mathbf B} = 0
 \\[2mm]
 \Leftrightarrow \quad & \omega m n_0 \tilde{\mathbf U} - \begin{pmatrix} 0 \\ k_y \\ k_z \end{pmatrix} \frac{\gamma p_0}{\omega}(k_y \tilde U_y + k_z \tilde U_z) - \frac{B_0^2}{\mu_0} \frac{1}{\omega} J M^{-1} J^\top \tilde{\mathbf U} = 0
 \\[2mm]
 \Leftrightarrow \quad & \omega^2 \tilde{\mathbf U} - c_s^2 
 \begin{pmatrix} 
  0 & 0 & 0
  \\[1mm]
  0 & k_y^2 & k_yk_z 
  \\[1mm]
  0 & k_yk_z & k_z^2 
 \end{pmatrix} \tilde{\mathbf U} - v_A^2 J M^{-1} J^\top \tilde{\mathbf U} = 0
 \end{align}
$$

which leads to the following dispersion relation:

$$
\textrm{det} \left[ \begin{pmatrix} 
  \omega^2 & 0 & 0
  \\[1mm]
  0 & \omega^2 - c_s^2k_y^2 & -c_s^2k_yk_z 
  \\[1mm]
  0 & -c_s^2k_yk_z & \omega^2 - c_s^2k_z^2 
 \end{pmatrix} - v_A^2 J M^{-1} J^\top \right] = 0\,.
$$

Moreover, 

$$
 J M^{-1} J^\top = \frac{1}{1 \red{+ a^2 + b^2}}
  \begin{pmatrix} 
  k_z^2 & \red{-k_z(a k_z + b k_y)} & 0
  \\[1mm]
  \red{k_z(a k_z + b k_y)} & k^2 
  \\[1mm]
  0 & 0 & 0
 \end{pmatrix}\,,
$$

such that the final dispersion relation reads

$$
\begin{equation}
\textrm{det} \begin{pmatrix} 
  \omega^2 - \frac{v_A^2 k_z^2}{1 \red{+ a^2 + b^2}} & \red{\frac{v_A^2 k_z(a k_z + b k_y)}{1 + a^2 + b^2}} & 0
  \\[2mm]
  \red{-\frac{v_A^2k_z(a k_z + b k_y)}{1 + a^2 + b^2}} & \omega^2 - c_s^2k_y^2 - \frac{v_A^2 k^2}{1 \red{+ a^2 + b^2}} & -c_s^2k_yk_z 
  \\[2mm]
  0 & -c_s^2k_yk_z & \omega^2 - c_s^2k_z^2 
 \end{pmatrix} = 0\,.
\end{equation}
$$

Without the Hall term (i.e. in ideal MHD) we have $M^{-1} = I$ and we can neglect the red terms to obtain 

$$
\textrm{ideal MHD:}\qquad
\textrm{det}  \begin{pmatrix} 
  \omega^2 - v_A^2 k_z^2 & 0 & 0
  \\[1mm]
  0 & \omega^2 - c_s^2k_y^2 - v_A^2 k^2 & -c_s^2k_yk_z 
  \\[1mm]
  0 & -c_s^2k_yk_z & \omega^2 - c_s^2k_z^2 
 \end{pmatrix} = 0\,.
$$

The corresponding characteristic polynomial reads

$$
 (\omega^2 - v_A^2 k_z^2)\left[\omega^4 - \omega^2( c_s^2 + v_A^2 ) k^2 + c_s^2 v_A^2 k_z^2 k^2 \right] = 0\,,
$$

The first factor yields

$$
 \textrm{Alfvén waves:}\qquad \omega = v_A k_z\,,
$$

whereas the second factor can be re-written as (with $x = \omega^2$)

$$
 x^2 - x( c_s^2 + v_A^2 ) k^2 + c_s^2 v_A^2 k_z^2 k^2 = 0 \,,
$$

which leads to the magnetosonic waves:

$$
\begin{align}
\textrm{fast magnetosonic wave:} \qquad \omega^2 = \frac 12 ( c_s^2 + v_A^2 ) k^2 \left( 1 + \sqrt{1 - \frac{4 c_s^2 v_A^2 \cos^2 \theta}{( c_s^2 + v_A^2 )^2}}\right)\,,
\\[2mm]
\textrm{slow magnetosonic wave:} \qquad \omega^2 = \frac 12 ( c_s^2 + v_A^2 ) k^2 \left( 1 - \sqrt{1 - \frac{4 c_s^2 v_A^2 \cos^2 \theta}{( c_s^2 + v_A^2 )^2}}\right)\,,
\end{align}
$$

where $k_z = k \cos \theta$ and $\theta$ is the angle between $\mathbf k$ and $\mathbf B$. This is precisley the dispersion relation implemented [here](https://struphy.pages.mpcdf.de/struphy/sections/subsections/dispersions.html#struphy.dispersion_relations.analytic.MHDhomogenSlab). 

Let us turn to the Hall MHD case, thus including the red terms in eq. (9). First we not that 

$$
 1 \red{+ a^2 + b^2} = 1 \red{- A^2\frac{k_z^2}{\omega^2} k^2}\,.
$$

We thus multiply the first and second row of the matrix by $\omega^2 (1 \red{+ a^2 + b^2})$ to obtain

$$
\begin{equation}
\textrm{det} \begin{pmatrix} 
  \omega^2(\omega^2 \red{- A^2k_z^2 k^2}) - \omega^2v_A^2 k_z^2 & \red{\omega^2 v_A^2 k_z(a k_z + b k_y)} & 0
  \\[2mm]
  \red{-\omega^2 v_A^2k_z(a k_z + b k_y)} & (\omega^2 - c_s^2k_y^2)(\omega^2 \red{- A^2k_z^2 k^2}) - \omega^2 v_A^2 k^2 & -c_s^2k_yk_z(\omega^2 \red{- A^2k_z^2 k^2}) 
  \\[2mm]
  0 & -c_s^2k_yk_z & \omega^2 - c_s^2k_z^2 
 \end{pmatrix} = 0\,.
\end{equation}
$$

We expand the first two diagonal elements:

$$
\begin{align}
\omega^2(\omega^2 \red{- A^2k_z^2 k^2}) - \omega^2v_A^2 k_z^2 &= \omega^4 - \omega^2(v_A^2 k_z^2 \red{+ A^2k_z^2 k^2})
\\[2mm]
(\omega^2 - c_s^2k_y^2)(\omega^2 \red{- A^2k_z^2 k^2}) - \omega^2 v_A^2 k^2 &= \omega^4 - \omega^2(c_s^2k_y^2 + v_A^2 k^2\red{+ A^2k_z^2 k^2})  \red{+ A^2k_z^2 k^2 c_s^2k_y^2}
\end{align}
$$

By introducing the abbreviation

$$
 \omega_0^2 := A^2 k_z^2 k^2\,,
$$ 

the characteristic polynomial can be written as

$$
\begin{gather*}
 \left[ \omega^4 - \omega^2(v_A^2 k_z^2 \red{+ \omega_0^2})\right] \Big\{ \Big[ \omega^4 - \omega^2(c_s^2k_y^2 + v_A^2 k^2\red{+ \omega_0^2})  \red{+ \omega_0^2 c_s^2k_y^2}\Big] (\omega^2 - c_s^2k_z^2) - c_s^4k_y^2k_z^2(\omega^2 \red{- \omega_0^2})  \Big\}
 \\[2mm]
 \qquad\red{ + \, \omega^4 v_A^4 k_z^2(a k_z + b k_y)^2 (\omega^2 - c_s^2k_z^2)} = 0 \,.
 \end{gather*}
$$

We have one further simplification of the last term,

$$
(a k_z + b k_y)^2 = (a k_z + b k_y)^2 + (a k_y - b k_z)^2 = (a^2 + b^2)k^2 = -A^2 \frac{k_z^2}{\omega^2}k^4 = - \omega_0^2\frac{k^2}{\omega^2}\,,
$$

such that, after dividing by $\omega^2$, the final Hall dispersion relation reads

$$
\begin{gather*}
 \left[ \omega^4 - \omega^2(v_A^2 k_z^2 \red{+ \omega_0^2})\right] \Big[ \omega^4 - \omega^2(c_s^2k^2 + v_A^2 k^2\red{+ \omega_0^2}) + v_A^2 k^2c_s^2k_z^2 \red{+ \omega_0^2 c_s^2k^2}  \Big] \red{ - \, \omega_0^2 v_A^4 k_z^2 k^2 (\omega^2 - c_s^2k_z^2)} = 0 \,.
 \end{gather*}
$$

Let us use `scipy` to find the four roots $x_i = (\omega^2)_i$ of this expresion:

```
from sympy import *

x, k, kz, vA, cs, om0_2 = symbols('omega^2, k, k_z, v_A, c_s, \omega_0^2')
p1 = x**2 - x*(vA**2*kz**2 + om0_2)
p2 = x**2 - x*(cs**2*k**2 + vA**2*k**2 + om0_2) + cs**2*k**2*(vA**2*kz**2 + om0_2)
p3 = (x - cs**2*kz**2)*om0_2*vA**4*kz**2*k**2
char_poly = p1 * p2 - p3
out = solve(char_poly, x, dict=True)
out[0]
```

returns

```
{omega^2: \omega_0^2}
```

Hence $\omega_0 = v_A^2 k_z k/\Omega_i$ is indeed the first root of the polynomial. It will be interesting to factor this root and study the remaining cubic equation:

```
pp = char_poly.as_poly(x)
fac = x - om0_2 
qq, rest = div(pp, fac.as_poly(x))
print(rest)
print(qq)
```

yields

```
Poly(0, omega^2, domain='RR[k,\omega_0^2,c_s,k_z,v_A]')
Poly(1.0*omega^2**3 + (-1.0*\omega_0^2 - 1.0*c_s**2*k**2 - 1.0*k**2*v_A**2 - 1.0*k_z**2*v_A**2)*omega^2**2 + (1.0*\omega_0^2*c_s**2*k**2 + 2.0*c_s**2*k**2*k_z**2*v_A**2 + 1.0*k**2*k_z**2*v_A**4)*omega^2 - 1.0*c_s**2*k**2*k_z**4*v_A**4, omega^2, domain='RR[k,\omega_0^2,c_s,k_z,v_A]')
```

Hence we obtain the following form of the characteristic polynomial:

$$
 (x - \omega_0^2) \Big\{ x^3 - x^2\Big[(c_s^2 + v_A^2)k^2 + v_A^2 k_z^2 + \omega_0^2\Big] + x\Big[c_s^2 k^2 (2 v_A^2 k_z^2 + \omega_0^2) + v_A^4 k_z^2 k^2 \Big] - c_s^2 v_A^4 k_z^4 k^2\Big\} = 0
$$

It will be interesting to study the discriminant of the above cubic equation, to see whether there can be instabilites. It turns out that we have to limit ourselves to the parallel case $k_z = k$ to be able to obtain analytical results. In this case, we find that the discriminant is zero for 

$$
 k^2 = \frac{\Omega_i^2}{c_s^2}\left(\frac{c_s^2}{v_A^2} - 1 \right)^2 = \frac{1}{\gamma\rho_i^2}(\gamma \beta - 1)^2\,,
$$

where $\beta = p_0 \mu_0 /B_0^2$ denotes the plasma beta and $\gamma = 5/3$ is the heat capacity ration for an ideal gas in 3d. We thus expect a stability threshold at such values of k. For low $\beta \ll 1$ this occurs at wavelength of the order of the ion Larmor radius $\rho_i = v_i/\Omega_i$, where $v_i = c_s /\sqrt \gamma$ denotes the ion thermal velocity. 

## Info

* Project executive(s): Nathan Mateo Marin Jimenez
* Duration: ongoing
* Context: PhD thesis
* Funding: Max Planck IPP
* Start date: 10/2023
* End date: open
* Supervisor: [Stefan Possanner](https://www.linkedin.com/in/stefan-possanner-52a1a250/)

## Links


## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)

 
