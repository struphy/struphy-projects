 <img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
 # Hamiltonian formulation of linearized kinetic- and gyrokinetic plasma models

Status: running since 03/2022.

## Topic

The aim is to investigate linearized versions of Vlasov-Maxwell and gyrokinetic plasma models and to find Hamiltonian formulations of these. This can be done in the framework of "Casimir equlibira" developed by Morrison and co-workers, albeit the space of such equilibria is maybe not big enough to cover interesting Physics. This is a question we shall clarify in this work.

Moreover, we plan Struphy implmentations of the newly found linear models and comparisons to full-f and delta-f models in terms of performance and accuracy. Once the linear models/codes are established, the goal is to extend to fully nonlinear models by re-adding the previously truncated nonlinear terms, thereby keeping the established "linear" Hamiltonian structure and enhancing it.

One system of interest is the electrostatic electron dynamics in static m

## Info

* Project executive(s): Dominik Bell
* Duration: ongoing
* Context: PhD thesis
* Funding: Max Planck IPP
* Start date: 03/2022
* End date: open
* Supervisor: Eric Sonnendrücker, [Stefan Possanner](https://www.linkedin.com/in/stefan-possanner-52a1a250/)

## Links

## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)

 
