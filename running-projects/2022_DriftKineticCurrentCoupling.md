 <img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
 # Driftkinetic-MHD hybrid models for the description of energetic particles in Tokamaks and Stellarators

Status: running since 07/2022.

## Topic

The aim is to discretize in Struphy a Hamiltonian hybrid model proposed in [Cesare Tronci 2020](https://iopscience.iop.org/article/10.1088/1361-6587/ab7c4d/meta), comprised of the full, ideal MHD equations coupled to a driftkinetic equation for energetic particles. The new model should be benchmarked against Struphy's full [Vlasov-MHD hybrid model](https://struphy.pages.mpcdf.de/struphy/sections/models.html#struphy.models.hybrid.LinearMHDVlasovCC) looking at the resonant excitation of toroidal Alfvén eigenmodes (TAEs). 

As a start, the MHD part of the model will be linearized, in order to focus on the driftkinetic part. Later, the fully nonlinear model will be addressed.

### Model equations
$$
\begin{align}
\textnormal{MHD} & \left\{
\begin{aligned}
\frac{\partial \rho}{\partial t} &+ \nabla \cdot (\rho \mathbf{U}) = 0 \,,
\\
\rho \frac{\partial \mathbf{U}}{\partial t} &+ \rho(\mathbf{U} \cdot \nabla)\mathbf{U} = \left(\frac{1}{\mu_0}\nabla\times\mathbf{B} + q_\textnormal{h} n_\textnormal{gc}\mathbf{U} - \mathbf{J}_\textnormal{gc} - \nabla\times\mathbf{M}_\textnormal{gc}\right) \times \mathbf{B} - \nabla p \,,
\\
\frac{\partial \mathbf{B}}{\partial t} &+ \nabla\times(\mathbf{B}\times\mathbf{U}) = 0 \,,
\\
\frac{\partial p}{\partial t} &+ \nabla\cdot(p\mathbf{U}) + (\gamma - 1)p\nabla\cdot\mathbf{U} = 0 \,,
\end{aligned} \right.
\\
\textnormal{Driftkinetic} &\left\{
\begin{aligned}
\,\,\,\,\frac{\partial f^\text{vol}}{\partial t} &+ \nabla_\mathbf{x} \cdot \left[\frac{f^\text{vol}}{B^*_\parallel}(v_\parallel \mathbf{B}^* - \mathbf{b}_0\times\mathbf{E}^*)\right] + \frac{q_\textnormal{h}}{m_\textnormal{h}}\nabla_{v_\parallel}\left[\frac{f^\text{vol}}{B^*_\parallel}\mathbf{B}^*\cdot\mathbf{E}^*\right] \,,
\\
n_\textnormal{gc} &= \iint f^\text{vol} \textnormal{d} v_\parallel \textnormal{d} \mu \,,
\\
\mathbf{J}_\textnormal{gc} &= q_\textnormal{h} \iint \frac{f^\text{vol}}{B^*_\parallel}(v_\parallel \mathbf{B}^* - \mathbf{b}_0 \times \mathbf{E}^*) \textnormal{d} v_\parallel \textnormal{d} \mu \,,
\\
\mathbf{M}_\textnormal{gc} &= -\iint f^\text{vol} \mu \mathbf{b}_0 \textnormal{d} v_\parallel \textnormal{d} \mu \,,
\end{aligned} \right.
\end{align}
$$
where the effective electric and magnetic fields are given by
$$
\begin{equation}
\mathbf{B}^* = \mathbf{B} + \frac{m_\textnormal{h}}{q_\textnormal{h}}v_\parallel\nabla\times\mathbf{b}_0 \,,
\qquad
\mathbf{E}^* = -\mathbf{U} \times \mathbf{B} - \frac{\mu}{q_\textnormal{h}}\nabla B_\parallel \,,
\end{equation}
$$
and the parallel magnetic fields are defined as
$$
\begin{equation}
B^*_\parallel = \mathbf{b}_0 \cdot \mathbf{B}^* = B_\parallel + \frac{m_\textnormal{h}}{q_\textnormal{h}}v_\parallel\mathbf{b}_0\cdot\nabla\times\mathbf{b}_0\,,
\qquad
B_\parallel = \mathbf{b}_0 \cdot \mathbf{B}\,.
\end{equation}
$$

### Lagrangian and Euler-Poincaré reduction
The model equations can be derived from the variational principle 
$$
\begin{equation}
\delta \mathcal{A} = \delta \int^{t_2}_{t_1}(L_\textnormal{\textnormal{p}} + L_\textnormal{MHD}) \, \textnormal{d} t = 0 \,,
\end{equation}
$$
where the Eulerian particle Lagrangian and MHD Lagrangian are defined as
$$
\begin{equation}
\begin{aligned}
L_\textnormal{\textnormal{p}}(f^\text{vol}, \mathcal{X}, \mathbf{A}, \phi) &= \int f^\text{vol}\left[ (m_\textnormal{h} v_\parallel \mathbf{b}_0 + q_\textnormal{h}\mathbf{A})\cdot\mathbf{u}_\textnormal{gc} - \frac{m_\textnormal{h}}{2}v²_\parallel - \mu B_\parallel - q_\textnormal{h} \phi\right]\, \textnormal{d} \mu \textnormal{d}^4 \mathbf{z}\,,
\\
L_\textnormal{MHD}(\rho, \mathbf{U}, \mathbf{A}, p) &= \int \frac{\rho}{2} |\mathbf{U}|² - \frac{p}{\gamma-1} - \frac{1}{2\mu_0}|\nabla\times\mathbf{A}|²\,\textnormal{d}³\mathbf{x}\,.
\end{aligned}
\end{equation}
$$
Here, we denote the Eulerian guiding-center phase-space coordinates by
$$
\begin{equation}
\mathbf{z} = (\mathbf{X}, v_\parallel) = \Phi^f_t(\mathbf{z}_0 = (\mathbf{X}_0, v_{\parallel,0}))\,,
\end{equation}
$$
and its Eulerian phase-space vector field
$$
\begin{equation}
\mathcal{X}(\mathbf{z},t)=(\mathbf{u}_\textnormal{gc}(\mathbf{z},t), a_\parallel(\mathbf{z},t)) \,,
\end{equation}
$$
where $\Phi^f_t$ denotes the phase-space Lagrangian particle flow map which is the solution of the following ODE
$$
\begin{equation}
\begin{aligned}
\dot{\mathbf{X}}(t) &= \mathbf{u}_\textnormal{gc}(\mathbf{z}, t)\,,
\\
\dot v_\parallel(t) &= a_\parallel(\mathbf{z},t) \,.
\end{aligned}
\end{equation}
$$
Similarly, Lagrangian MHD fluid flow map can also be defined as $\Phi^\text{MHD}_t$ and the two Eulerian vector fields are given by
$$
\begin{equation}
\begin{aligned}
\dot \Phi^f_t(\mathbf{z}_0,t) &= \mathcal{X}(\Phi^f_t(\mathbf{z}_0,t),t) \,,
\\
\dot \Phi^\text{MHD}_t(\mathbf{x}_0,t) &= \mathbf{U}(\Phi^\text{MHD}_t(\mathbf{x}_0,t),t)\,.
\end{aligned}
\end{equation}
$$
According to the Euler-Poincaré reduction theorem (see [Stefan's lecture notes](https://gitlab.mpcdf.mpg.de/spossann/variational-plasma/-/blob/main/lecture_notes.pdf?ref_type=heads), pages 36, Theorem 15), two Eulerian vector fields $\mathcal{X}$ and $\mathbf{U}$ have the following variational relations
$$
\begin{equation}
\delta\mathbf{U} = \partial_t \boldsymbol \eta + (\mathbf{U} \cdot \nabla) \boldsymbol \eta  - (\boldsymbol \eta \cdot \nabla)\mathbf{U} \,, \qquad \delta\mathcal{X} = \partial_t \boldsymbol{\xi}  + (\mathcal{X} \cdot \nabla_\mathbf{z})\boldsymbol{\xi}  - (\boldsymbol{\xi} \cdot \nabla_\mathbf{z})\mathcal{X} \,,
\end{equation}
$$
where $\boldsymbol{\eta}$ and $\boldsymbol{\xi}$ are arbitary variations of flow map satisfying $\delta \Phi^\text{MHD}_t(\mathbf{x}_0,t) = \boldsymbol{\eta}$ and $\delta \Phi^f_t(\mathbf{z}_0,t)=\boldsymbol{\xi}$. Three additional variational relations can be derived as in the standard approach by Newcomb. Thus, the variables $\rho$, $f^\text{vol}$ and $\mathbf{A}$ satisfy following Eulerian-Lagrangian relations
$$
\begin{equation}
\begin{aligned}
\rho(\Phi^\text{MHD}_t(\mathbf{x}_0,t),t) \, \textnormal{d}³\Phi^\text{MHD}_t &= \rho_0(\mathbf{x}_0) \, \textnormal{d}³\mathbf{x}_0 \,,
\\
\mathbf{A}(\Phi^\text{MHD}_t(\mathbf{x}_0,t),t)\cdot\textnormal{d}³\Phi^\text{MHD}_t &= \mathbf{A}_0(\mathbf{x}_0)\cdot\textnormal{d}³\mathbf{x}_0 \,,
\\
f^\text{vol}(\Phi^f_t(\mathbf{z}_0,t)) \, \textnormal{d}³\Phi^f_t &= f^\text{vol}_0(\mathbf{z}_0) \, \textnormal{d}³\mathbf{z}_0 \,.
\end{aligned}
\end{equation}
$$
The above relations can be understood as the transport of differential forms (see [Stefan's lecture notes](https://gitlab.mpcdf.mpg.de/spossann/variational-plasma/-/blob/main/lecture_notes.pdf?ref_type=heads), pages 42-43). By taking time derivatives, the advection equations can be obtained
$$
\begin{equation}
\begin{aligned}
\partial_t \rho + \nabla\cdot(\rho\mathbf{U}) &= 0 \,,
\\
\partial_t \mathbf{A} + \mathbf{U}\cdot\nabla\mathbf{A} + \nabla\mathbf{U}\cdot\mathbf{A} &= 0 \,,
\\
\partial_t f^\text{vol} + \nabla_\mathbf{z}\cdot(f^\text{vol}\mathcal{X}) &= 0 \,,
\end{aligned}
\end{equation}
$$
and corresponding variations can be written as
$$
\begin{equation}
\begin{aligned}
\delta \rho &= -\nabla\cdot(\rho\boldsymbol{\eta})\,, \qquad \delta \mathbf{A} = -\boldsymbol{\eta}\cdot\nabla\mathbf{A} - \nabla\boldsymbol{\eta}\cdot\mathbf{A}\,, \qquad \delta f^\text{vol} &= -\nabla_\mathbf{z}\cdot(f^\text{vol}\boldsymbol{\xi})\,.
\end{aligned}
\end{equation}
$$
In terms of pressure, standard adiabatic MHD closure is considered
$$
\begin{equation}
\frac{\textnormal{d}}{\textnormal{d} t}\left(\frac{p}{\rho^\gamma}\right) = \partial_t p + \nabla\cdot(p\mathbf{U}) + (\gamma - 1)p\nabla\cdot\mathbf{U} = 0 \,,
\end{equation}
$$
where the corresponding variations are
$$
\begin{equation}
\delta p = -\boldsymbol{\eta} \cdot \nabla p - \gamma p \nabla \cdot \boldsymbol{\eta} \,.
\end{equation}
$$
With the constrained variations, variational principle of the action (5) gives:
$$
\begin{equation}
\begin{aligned}
- \boldsymbol{\eta} \cdot &\left[\rho(\partial_t + \mathbf{U} \cdot \nabla)\mathbf{U} + \nabla p - (\frac{1}{\mu_0}\nabla\times\mathbf{B} + q_\textnormal{h} n_\textnormal{gc} \mathbf{U} - \mathbf{J}_\textnormal{gc} - \nabla \times \mathbf{M}_\textnormal{gc})\times \mathbf{B} \right]
\\
- \boldsymbol{\xi}_\mathbf{X} \cdot &\left[m_\textnormal{h} \partial_t v_\parallel \mathbf{b}_0 + q_\textnormal{h} \partial_t \mathbf{A} + \mathbf{u}_\textnormal{gc} \cdot\nabla(m_\textnormal{h} v_\parallel \mathbf{b}_0 + q_\textnormal{h} \mathbf{A}) + \nabla\mathbf{u}_\textnormal{gc}\cdot(m_\textnormal{h} v_\parallel \mathbf{b}_0 + q_\textnormal{h} \mathbf{A}) \right.
\\
&\left. + \nabla(m_\textnormal{h} v_\parallel\mathbf{b}_0\cdot\mathbf{u}_\textnormal{gc} + q_\textnormal{h}\mathbf{A}\cdot\mathbf{u}_\textnormal{gc} - \mu_\parallel B_\parallel - q_\textnormal{h} \mathbf{U} \cdot \mathbf{A})\right]
\\
+\boldsymbol{\xi}_{v_\parallel} \cdot &\left[\mathbf{b}_0 \cdot \mathbf{u}_\textnormal{gc} - v_\parallel\right] = 0\,,
\end{aligned}
\end{equation}
$$
where
$$
\begin{equation}
\begin{aligned}
\mathbf{J}_\textnormal{gc} &= \frac{\delta L_p}{\delta \mathbf{A}} = q_\textnormal{h} \iint f^\text{vol} \mathbf{u}_\textnormal{gc} \textnormal{d} v_\parallel \textnormal{d} \mu \,,
\\
\mathbf{M}_\textnormal{gc} &= \frac{\delta L_p}{\delta \mathbf{B}} = - \iint f^\text{vol} \mu \mathbf{b}_0 \textnormal{d} v_\parallel \textnormal{d} \mu \,.
\end{aligned}
\end{equation}
$$
First line of the equation (17) directly gives the momentum balance equation of MHD fluid with the three coupling terms
$$
\begin{equation}
\rho \frac{\partial \mathbf{U}}{\partial t}+ \rho\mathbf{U} \cdot \nabla\mathbf{U} = (\frac{1}{\mu_0}\nabla\times\mathbf{B} + q_\textnormal{h} n_\textnormal{gc} \mathbf{U} - \mathbf{J}_\textnormal{gc} - \nabla \times \mathbf{M}_\textnormal{gc})\times \mathbf{B} - \nabla p \,.
\end{equation}
$$
 By replacing $\partial_t \mathbf{A}$ in the second line of the equation (17) with the second in equation (13), one can derive guiding center equation
$$
\begin{equation}
q_\textnormal{h} (\mathbf{E}^* + \mathbf{u}_\textnormal{gc} \times \mathbf{B}^*) = m_\textnormal{h} a_{\parallel \textnormal{gc}} \mathbf{b}_0 \,.
\end{equation}
$$
Corresponding equations of motion can be obtained from the Euler-Lagrange equations of $L_\textnormal{p}$
$$
\begin{equation}
\begin{aligned}
\mathcal{X} = (\mathbf{u}_\textnormal{gc}, a_{\parallel \textnormal{gc}}) = \left(\frac{1}{B^*_\parallel}(v_\parallel \mathbf{B}^* - \mathbf{b}_0 \times \mathbf{E}^*) , \,\frac{1}{B^*_\parallel} \mathbf{B}^* \cdot \mathbf{E}^* \right)\,.
\end{aligned}
\end{equation}
$$
By plugging the obtained vector field $\mathcal{X}$ into third in equation (13), one can obtain transport equation of guiding-center kinetic density as a volume-form
$$
\begin{equation}
\frac{\partial f^\text{vol}}{\partial t} + \nabla_\mathbf{x} \cdot \left[\frac{f^\text{vol}}{B^*_\parallel}(v_\parallel \mathbf{B}^* - \mathbf{b}_0\times\mathbf{E}^*)\right] + \frac{q_\textnormal{h}}{m_\textnormal{h}}\nabla_{v_\parallel}\left[\frac{f^\text{vol}}{B^*_\parallel}\mathbf{B}^*\cdot\mathbf{E}^*\right] \,,
\end{equation}
$$
or can also be written as a transport equation with the $f = f^\text{vol}/B_\parallel^*$
$$
\begin{equation}
\frac{\partial f}{\partial t}+ \frac{1}{B_\parallel^*}(v_\parallel \mathbf{B}^* - \mathbf{b}_0\times\mathbf{E}^*)\cdot\nabla f + \frac{q_\textnormal{h}}{m_\textnormal{h}} \frac{1}{B_\parallel^*}(\mathbf{B}^* \cdot \mathbf{E}^*)\nabla_{v_\parallel}f = 0\,.
\end{equation}
$$
Here, we have used the Liouville's theorem with the jacobian of the guiding-center coordinate transformation, $B_\parallel^*$,
$$
\begin{equation}
\frac{\partial B_\parallel^*}{\partial t} + \nabla\cdot(B_\parallel^* \mathbf{u}_\textnormal{gc}) + \nabla_{v_\parallel}(B_\parallel^* a_{\parallel, \textnormal{gc}}) \,.
\end{equation}
$$

### Noether's theorem
According to Noether's theorem (see for instance the appendix of [Sugama 2000](https://pubs.aip.org/aip/pop/article-abstract/7/2/466/457925/Gyrokinetic-field-theory)), when the Lagrangian of the system $L$ does not explicitly depend on time, the total energy of the system is conserved in time. In case of the Lagrangian defined in the equation (6), the following total energy is conserved
$$
\begin{equation}
\begin{aligned}
\mathcal{E}_\text{tot} &= \int \dot{\mathbf{z}} \cdot \frac{\partial \mathcal{L}}{\partial \dot{\mathbf{z}}}\textnormal{d}^4\mathbf{z} + \int \dot{\mathbf{x}} \cdot \frac{\partial \mathcal{L}}{\partial \dot{\mathbf{x}}}\textnormal{d}^3\mathbf{x} - L \,,
\\
&= \int f^\text{vol} \left[ \frac{m_\textnormal{h}}{2}v_\parallel^2 + \mu B_\parallel\right] \textnormal{d} \mu \textnormal{d}^4 \mathbf{z} + \int \frac{\rho}{2}|\mathbf{U}|^2 + \frac{p}{\gamma -1} + \frac{1}{2\mu_0}|\nabla \times \mathbf{A}|^2 \textnormal{d}^3 \mathbf{x} \,,
\\
&=\underbrace{ \int \frac{\rho}{2}\mathbf{U}^2  \textnormal{d}^3 \mathbf{x}}_{\mathcal{E}_U}
+ \underbrace{ \int \frac{1}{2\mu_0}\mathbf{B}^2  \textnormal{d}^3 \mathbf{x}}_{\mathcal{E}_B} 
+ \underbrace{ \int \frac{p}{\gamma - 1}  \textnormal{d}^3 \mathbf{x}}_{\mathcal{E}_T} 
+ \underbrace{\int f^\text{vol} \frac{m_\textnormal{h}}{2} v_\parallel^2 \textnormal{d} v_\parallel \textnormal{d} \mu \textnormal{d}^3 \mathbf{x}}_{\mathcal{E}_{fv}} 
+ \underbrace{\int f^\text{vol} \mu B_\parallel \textnormal{d} v_\parallel \textnormal{d} \mu \textnormal{d}^3 \mathbf{x} }_{\mathcal{E}_{fB}}  \,.
\end{aligned}
\end{equation}
$$
where $\mathcal L$ is the Lagrange density such that
$$
 L = \int \mathcal L\,\textnormal{d}^4 \mathbf{z} \quad \text{or} \quad \int \mathcal L \, \textnormal{d}^3 \mathbf{x} \,.
$$
Thus, the Lagrangian $L$ depends on time only through the $\mathbf{z}(t)$ and $\mathbf{x}(t)$, so then the action $\mathcal{A}$ is invariant (symmetric) under the time variation.

Proof of the energy conservation :


The evolution of the bulk internal energy reads
$$
\begin{equation*}
\begin{aligned}
    \frac{\textnormal{d}}{\textnormal{d} t}\mathcal{E}_T &= \frac{1}{\gamma - 1} \int \frac{\partial p}{\partial t} \textnormal{d}^3 \mathbf{x}
    \\
    &= \frac{1}{\gamma - 1} \int - \nabla \cdot (p \mathbf{U}) - (\gamma - 1) p \nabla \cdot \mathbf{U} \textnormal{d}^3 \mathbf{x}
    \\
    &= \frac{1}{\gamma - 1} \int - \nabla p \cdot \mathbf{U} - p (\nabla \cdot \mathbf{U}) - (\gamma - 1) p \nabla \cdot \mathbf{U} \textnormal{d}^3 \mathbf{x}
    \\
    &= \underbrace{\int  \nabla p \cdot \mathbf{U} \textnormal{d}^3 \mathbf{x}}_1
\end{aligned}
\end{equation*}
$$
The evolution of the bulk kinetic energy reads
$$
\begin{equation*}
\begin{aligned}
\frac{\textnormal{d}}{\textnormal{d} t} \mathcal{E}_U &= \int \frac{1}{2} \frac{\partial \rho}{\partial t}\mathbf{U}^2 + \rho \frac{\partial \mathbf{U}}{\partial t} \cdot \mathbf{U} \textnormal{d}^3 \mathbf{x}
\\
&= \int \cancel{\frac{1}{2} \frac{\partial \rho}{\partial t} \mathbf{U}^2} - \cancel{\rho (\mathbf{U} \cdot \nabla) \mathbf{U} \cdot \mathbf{U}} + \left[ \frac{1}{\mu_0} (\nabla \times \mathbf{B}) \times \mathbf{B}- \mathbf{J}_{gc} \times \mathbf{B} - (\nabla \times \mathbf{M}_{gc}) \times \mathbf{B} - \nabla p \right] \cdot \mathbf{U} \textnormal{d}^3 \mathbf{x}
\\
&= \underbrace{\frac{1}{\mu_0} \int (\nabla \times \mathbf{B}) \times \mathbf{B} \cdot \mathbf{U} \textnormal{d}^3 \mathbf{x}}_2 - \underbrace{\int \nabla p \cdot \mathbf{U} \textnormal{d}^3 \mathbf{x}}_1
\\
&-\underbrace{m_h \int \frac{f^\text{vol}}{B^*_\parallel} v^2_\parallel (\nabla \times \mathbf{b}_0) \times \mathbf{B} \cdot \mathbf{U} \textnormal{d} v_\parallel \textnormal{d} \mu \textnormal{d}^3 \mathbf{x}}_3 
- q_h \cancel{\int \frac{f^\text{vol}}{B^*_\parallel} \mathbf{b}_0 \times (\mathbf{U} \times \mathbf{B}) \times \mathbf{B} \cdot \mathbf{U} \textnormal{d} v_\parallel \textnormal{d} \mu \textnormal{d}^3 \mathbf{x}}
\\
&-\underbrace{\int \frac{f^\text{vol}}{B^*_\parallel} \mu (\mathbf{b}_0 \times \nabla B_\parallel ) \times \mathbf{B} \cdot \mathbf{U} \textnormal{d} v_\parallel \textnormal{d} \mu \textnormal{d}^3 \mathbf{x}}_4 + \underbrace{\int f^\text{vol} \mu (\nabla \times \mathbf{b}_0) \times \mathbf{B} \cdot \mathbf{U} \textnormal{d} v_\parallel \textnormal{d} \mu \textnormal{d}^3 \mathbf{x}}_5
\end{aligned}
\end{equation*}
$$
The evolution of the magnetic energy reads
$$
\begin{equation*}
    \begin{aligned}
        \frac{\textnormal{d}}{\textnormal{d} t} \mathcal{E}_B &= \frac{1}{\mu_0} \int \frac{\partial \mathbf{B}}{\partial t} \cdot \mathbf{B} \textnormal{d}^3 \mathbf{x}
        \\
        &= - \frac{1}{\mu_0} \int \nabla \times (\mathbf{B} \times \mathbf{U}) \cdot \mathbf{B} \textnormal{d}^3 \mathbf{x}
        \\
        &= \underbrace{\frac{1}{\mu_0} \int \mathbf{B} \times (\nabla \times \mathbf{B}) \cdot \mathbf{U} \textnormal{d}^3 \mathbf{x}}_2
    \end{aligned}
\end{equation*}
$$
The evolution of the particle kinetic energy reads
$$
\begin{equation*}
    \begin{aligned}
        \frac{\textnormal{d}}{\textnormal{d} t} \mathcal{E}_{fv} &= \frac{m_h}{2}\int v_\parallel^2 \frac{\partial f^\text{vol}}{\partial t} \textnormal{d} v_\parallel \textnormal{d} \mu \textnormal{d}^3 \mathbf{x} 
        \\
        &= -\frac{q_h}{2}\int v_\parallel^2 \nabla_{v_\parallel}\left[ \frac{f^\text{vol}}{B^*_\parallel} \mathbf{B}^* \cdot \mathbf{E}^* \right] \textnormal{d} v_\parallel  \textnormal{d} \mu \textnormal{d}^3 \mathbf{x}
        \\
        &= q_h \int v_\parallel  \frac{f^\text{vol}}{B^*_\parallel} \left[ \mathbf{B} + \frac{m_h}{q_h} v_\parallel \nabla \times \mathbf{b}_0 \right] \cdot \left[- \mathbf{U} \times \mathbf{B} - \frac{\mu}{q_h} \nabla B_\parallel \right] \textnormal{d} v_\parallel  \textnormal{d} \mu \textnormal{d}^3 \mathbf{x}
        \\
        &= q_h \cancel{\int  \frac{f^\text{vol}}{B^*_\parallel} v_\parallel \mathbf{B} \cdot (-\mathbf{U} \times \mathbf{B}) \textnormal{d} v_\parallel \textnormal{d} \mu \textnormal{d}^3 \mathbf{x}}
        + \underbrace{m_h \int  \frac{f^\text{vol}}{B^*_\parallel} v_\parallel^2 \mathbf{U} \cdot (\nabla \times \mathbf{b}_0) \times \mathbf{B} \textnormal{d} v_\parallel  \textnormal{d} \mu \textnormal{d}^3 \mathbf{x}}_3
        \\
        &- \underbrace{\int  \frac{f^\text{vol}}{B^*_\parallel} v_\parallel \mathbf{B} \cdot \nabla B_\parallel \mu \textnormal{d} v_\parallel  \textnormal{d} \mu \textnormal{d}^3 \mathbf{x}}_6 
        + \underbrace{\frac{m_h}{q_h} \int  \frac{f^\text{vol}}{B^*_\parallel} v_\parallel^2 (\nabla \times \mathbf{b}_0) \cdot \nabla B_\parallel \textnormal{d} v_\parallel  \textnormal{d} \mu \textnormal{d}^3 \mathbf{x}}_7
    \end{aligned}
\end{equation*}
$$
The evolution of the particle magnetic energy reads
$$
\begin{equation*}
    \begin{aligned}
        \frac{\textnormal{d}}{\textnormal{d} t} \mathcal{E}_{fB} &= \int \frac{\partial f^\text{vol}}{\partial t} \mu B_\parallel + f^\text{vol} \mu \frac{\partial B_\parallel}{\partial t} \textnormal{d} v_\parallel \textnormal{d} \mu \textnormal{d}^3 \mathbf{x}
        \\
        &= - \int \nabla_\mathbf{x}\cdot \left[ \frac{f^\text{vol}}{B^*_\parallel} (v_\parallel \mathbf{B}^* - \mathbf{b}_0 \times \mathbf{E}^*)\right] \mu B_\parallel + f^\text{vol} \mu \mathbf{b}_0 \cdot \nabla \times (\mathbf{B} \times \mathbf{U}) \textnormal{d} v_\parallel \textnormal{d} \mu \textnormal{d}^3 \mathbf{x}
        \\
        &= \int \left[  \frac{f^\text{vol}}{B^*_\parallel}(v_\parallel \mathbf{B} + v_\parallel^2 \frac{m_h}{q_h} \nabla \times \mathbf{b}_0 + \mathbf{b}_0 \times (\mathbf{U} \times \mathbf{B}) \right] \cdot \mu \nabla B_\parallel - f^\text{vol} \mu \mathbf{b}_0 \cdot \nabla \times (\mathbf{B} \times \mathbf{U})  \textnormal{d} v_\parallel \textnormal{d} \mu \textnormal{d}^3 \mathbf{x}
        \\
        &=\int \underbrace{\frac{f^\text{vol}}{B^*_\parallel} v_\parallel \mu \mathbf{B} \cdot \nabla B_\parallel}_6 +  \underbrace{\frac{f^\text{vol}}{B^*_\parallel} v_\parallel^2 \mu \frac{m_h}{q_h} (\nabla \times \mathbf{b}_0) \cdot \nabla B_\parallel}_7 \textnormal{d} v_\parallel \textnormal{d} \mu \textnormal{d}^3 \mathbf{x}
        \\
        &+ \int  \underbrace{\mu \frac{f^\text{vol}}{B^*_\parallel} \mathbf{U} \cdot \mathbf{B} \times (\nabla B_\parallel \times \mathbf{b}_0)}_4 - \underbrace{f^\text{vol} \mu \mathbf{b}_0 \cdot \nabla \times (\mathbf{B} \times \mathbf{U})}_5 \textnormal{d} v_\parallel \textnormal{d} \mu \textnormal{d}^3 \mathbf{x}
    \end{aligned}
\end{equation*}
$$
Taking the sum of all the time derivatives of the energies yields
$$
\begin{equation}
    \frac{\textnormal{d}}{\textnormal{d} t} \mathcal{E}_{\text{total}} = \frac{\textnormal{d}}{\textnormal{d} t} \mathcal{E}_U + \frac{\textnormal{d}}{\textnormal{d} t} \mathcal{E}_B + \frac{\textnormal{d}}{\textnormal{d} t} \mathcal{E}_T + \frac{\textnormal{d}}{\textnormal{d} t} \mathcal{E}_{fv} + \frac{\textnormal{d}}{\textnormal{d} t} \mathcal{E}_{fB} = 0 \,.
\end{equation}
$$

### Linearization of the MHD part
As a first MHD-Driftkinetic model, we consider Hybrid LinearMHD-Driftkinetic system which ignores only MHD non-linearities but keeps having non-linear couplings between MHD and kinetics. f^\text{vol}irst, we apply the zero-flow ansatz to the MHD variables
$$
\begin{equation}
\rho = \rho_0 + \tilde \rho \,, \quad \mathbf{U} = \tilde{\mathbf{U}} \,,  \quad \mathbf{B} = \mathbf{B}_0 + \tilde{\mathbf{B}} \,, \quad p = p_0 + \tilde p \,,
\end{equation}
$$
and then neglect all the nonlinear MHD contributions. As a results, following LinearMHD-Driftkinetic system can be obtained
$$
\begin{align}
\textnormal{LinearMHD} & \left\{
\begin{aligned}
\frac{\partial \tilde \rho}{\partial t} &+ \nabla \cdot (\rho_0 \tilde{\mathbf{U}}) = 0 \,,
\\
\rho_0 \frac{\partial \tilde{\mathbf{U}}}{\partial t} &= \frac{1}{\mu_0}(\nabla\times\mathbf{B}_0) \times \tilde{\mathbf{B}} + \frac{1}{\mu_0}(\nabla\times\tilde{\mathbf{B}}) \times \mathbf{B}_0 
\\
&+ \left(q_\textnormal{h} n_\textnormal{gc} \tilde{\mathbf{U}} - \mathbf{J}_\textnormal{gc} - \nabla\times\mathbf{M}_\textnormal{gc}\right) \times \mathbf{B} - \nabla \tilde p \,,
\\
\frac{\partial \tilde{\mathbf{B}}}{\partial t}&+ \nabla\times(\mathbf{B}_0 \times \tilde{\mathbf{U}}) = 0 \,,
\\
\frac{\partial \tilde p}{\partial t} &+ \nabla\cdot(p_0\tilde{\mathbf{U}}) + (\gamma - 1)p_0\nabla\cdot\tilde{\mathbf{U}} = 0 \,,
\end{aligned} \right.
\\
\textnormal{Driftkinetic} &\left\{
\begin{aligned}
\,\,\,\,\frac{\partial f^\text{vol}}{\partial t} &+ \nabla_\mathbf{x} \cdot \left[\frac{f^\text{vol}}{B^*_\parallel}(v_\parallel \mathbf{B}^* - \mathbf{b}_0\times\mathbf{E}^*)\right] + \frac{q_\textnormal{h}}{m_\textnormal{h}}\nabla_{v_\parallel}\left[\frac{f^\text{vol}}{B^*_\parallel}\mathbf{B}^*\cdot\mathbf{E}^*\right] \,,
\\
n_\textnormal{gc} &= \iint f^\text{vol} \textnormal{d} v_\parallel \textnormal{d} \mu \,,
\\
\mathbf{J}_\textnormal{gc} &= q_\textnormal{h} \iint \frac{f^\text{vol}}{B^*_\parallel}(v_\parallel \mathbf{B}^* - \mathbf{b}_0 \times \mathbf{E}^*) \textnormal{d} v_\parallel \textnormal{d} \mu \,,
\\
\mathbf{M}_\textnormal{gc} &= -\iint f^\text{vol} \mu \mathbf{b}_0 \textnormal{d} v_\parallel \textnormal{d} \mu \,,
\end{aligned} \right.
\end{align}
$$
where the effective fields and parallel magnetic fields are given by
$$
\begin{equation}
\mathbf{B}^* = \mathbf{B} + \frac{m_\textnormal{h}}{q_\textnormal{h}}v_\parallel\nabla\times\mathbf{b}_0 \,,
\qquad
\mathbf{E}^* = -\tilde{\mathbf{U}} \times \mathbf{B} - \frac{\mu}{q_\textnormal{h}}\nabla (B_\parallel) \,,
\end{equation}
$$
$$
\begin{equation}
B_\parallel^* = \mathbf{b}_0 \cdot \mathbf{B}^* = B_\parallel + \frac{m_\textnormal{h}}{q_\textnormal{h}}v_\parallel \mathbf{b}_0 \cdot (\nabla \times \mathbf{b}_0) \,, \qquad B_\parallel = B_0 + \mathbf{b}_0 \cdot \tilde{\mathbf{B}} \,.
\end{equation}
$$
As can be seen from the momentum balance equation of the equaiton (26), the coupling are stil fully non-linear.

However, as a consequence of the linearization, the total energy of the system $\mathcal{E}_\text{total}$ (equation (25))is no longer conserved, but the linearized energy
$$
\begin{equation}
\mathcal{E}_\text{lin} := \int \frac{\rho_0}{2} \tilde{\mathbf{U}}² + \frac{1}{2\mu}\tilde{\mathbf{B}}² + \frac{\tilde p}{\gamma - 1} + \iint f^\text{vol} \frac{m_\textnormal{h}}{2}v²_\parallel + f^\text{vol} \mu B_\parallel \textnormal{d} v_\parallel \textnormal{d} \mu \textnormal{d}³ \mathbf{x} \,,
\end{equation}
$$
evolves in time:
$$
\begin{equation}
\dot{\mathcal{E}}_\text{lin} = \int \tilde{\mathbf{U}} \cdot \left[\nabla \times \mathbf{B}_0 - \nabla \times \mathbf{M}_\textnormal{gc} \right] \times \tilde{\mathbf{B}}  - (p_0 - \tilde p) \nabla \cdot \tilde{\mathbf{U}} \, \textnormal{d}^3 \mathbf{x} \,.
\end{equation}
$$

### Normalization
In STRUPHY framework, all variables are normalized to some characteristic value which is denoted by over-hat notation. Firt of all following three independent units are defined
$$
\begin{equation}
\mathbf{x} = \mathbf{x}' \hat L\,, \qquad n_b = n_b' \hat n\,, \qquad \mathbf{B} = \mathbf{B}' \hat B\,.
\end{equation}
$$
Then all the other units can be expressed from the above three units
$$
\begin{equation}
\begin{aligned}
&\mathbf{U} = \mathbf{U}' \hat v\,, \qquad \hat v = \hat v_A = \frac{\hat B}{\sqrt{A_b m_H \hat n \mu_0}} \,,
\\
&t = t' \hat t\,, \qquad \hat t = \hat \tau_A = \frac{\hat L}{\hat v_A} \,, \qquad \hat \omega = \frac{2\pi}{\hat t}
\\
&\rho = n_b' A_b m_H \hat n \,, \qquad p = p' \frac{\hat B^2}{\mu_0}\,,
\\
&v_\parallel = v_\parallel' \hat v \,, \qquad \mu = \mu' \hat \mu \,, \qquad \hat \mu = \frac{A_h m_H \hat v_A^2}{\hat B}\,,
\\
&f^\text{vol} = f^{\text{vol}'} \frac{\hat n}{\hat v \hat \mu} \,, \qquad n_\textnormal{gc} = n_\textnormal{gc}' \hat n\,
\\
&J_\textnormal{gc} = J_\textnormal{gc}' Z_h e \hat n \hat v_A \,, \qquad M_\textnormal{gc} = M'_\textnormal{gc} \hat n \hat \mu \,.
\\
&\epsilon = \frac{\hat \omega}{2\pi\hat \Omega_{\textnormal{ch}}}\,, \qquad \hat \Omega_{\textnormal{ch}} = \frac{Z_\textnormal{h} e \hat B}{A_\textnormal{h} m_H}
\end{aligned}
\end{equation}
$$
Plugging the given normalizations into equation (28) - (31) yields
$$
\begin{align}
\textnormal{LinearMHD} & \left\{
\begin{aligned}
\frac{\partial \tilde n_b'}{\partial t'} &+ \nabla_{\mathbf{x}'} \cdot (n_{b,0}' \tilde{\mathbf{U}}') = 0 \,,
\\
n'_{b,0} \frac{\partial\tilde{\mathbf{U}}'}{\partial t'} &= (\nabla_{\mathbf{x}'}\times\mathbf{B}_0') \times \tilde{\mathbf{B}}' + (\nabla_{\mathbf{x}'}\times\tilde{\mathbf{B}}') \times \mathbf{B}_0' 
\\
&+ \frac{A_\textnormal{h}}{A_b}\left(n_\textnormal{gc}' \frac{1}{\epsilon}\tilde{\mathbf{U}}' - \frac{1}{\epsilon}\mathbf{J}_\textnormal{gc}' - \nabla_{\mathbf{x}'}\times\mathbf{M}_\textnormal{gc}'\right) \times \mathbf{B}' - \nabla_{\mathbf{x}'} \tilde p' \,,
\\
\frac{\partial\tilde{\mathbf{B}}'}{\partial t'} &+ \nabla_{\mathbf{x}'}\times(\mathbf{B}_0' \times \tilde{\mathbf{U}}') = 0 \,,
\\
\frac{\partial\tilde p'}{\partial t'} &+ \nabla_{\mathbf{x}'}\cdot(p_0'\tilde{\mathbf{U}}') + (\gamma - 1)p_0'\nabla_{\mathbf{x}'}\cdot\tilde{\mathbf{U}}' = 0 \,,
\end{aligned} \right.
\\
\textnormal{Driftkinetic} &\left\{
\begin{aligned}
\,\,\,\,\frac{\partial f^{\text{vol}'}}{\partial t'} &+ \nabla_{\mathbf{x}'} \cdot \left[\frac{f^{\text{vol}'}}{B^{*'}_\parallel}(v_\parallel' \mathbf{B}^{*'} - \mathbf{b}_0\times\mathbf{E}^{*'})\right] + \frac{1}{\epsilon}\nabla_{v'_\parallel}\left[\frac{f^{\text{vol}'}}{B^{*'}_\parallel}\mathbf{B}^{*'}\cdot\mathbf{E}^{*'}\right] \,,
\\
n_\textnormal{gc}' &= \iint f^{\text{vol}'} \textnormal{d} v_\parallel' \textnormal{d} \mu' \,,
\\
\mathbf{J}_\textnormal{gc}' &= \iint \frac{f^{\text{vol}'}}{B^{*'}_\parallel}(v_\parallel' \mathbf{B}^{*'} - \mathbf{b}_0 \times \mathbf{E}^{*'}) \textnormal{d} v_\parallel' \textnormal{d} \mu' \,,
\\
\mathbf{M}_\textnormal{gc}' &= -\iint f^{\text{vol}'} \mu' \mathbf{b}_0 \textnormal{d} v_\parallel' \textnormal{d} \mu' \,,
\end{aligned} \right.
\end{align}
$$
where 
$$
\begin{equation}
\mathbf{B}^{*'} = \mathbf{B}' + \epsilon v_\parallel'\nabla_{\mathbf{x}'}\times\mathbf{b}_0 \,,
\qquad
\mathbf{E}^{*'} = -\tilde{\mathbf{U}}' \times \mathbf{B}' - \epsilon \mu'\nabla_{\mathbf{x}'} (B_\parallel') \,,
\end{equation}
$$
$$
\begin{equation}
B_\parallel^{*'} = \mathbf{b}_0 \cdot \mathbf{B}^{*'} = B_\parallel' + \epsilon v_\parallel' \mathbf{b}_0 \cdot (\nabla_{\mathbf{x}'} \times \mathbf{b}_0) \,, \qquad B_\parallel' = B_0' + \mathbf{b}_0 \cdot \tilde{\mathbf{B}}' \,.
\end{equation}
$$

### Solution spaces
In order to solve the MHD equations with FEEC method, spaces of all the field variables should be one of the four De Rham spaces $H^1, \,H(\text{curl}), \,H(\text{div})$ or $L^2$. 

In this model, plasma densiy $n$ and pressure $p$ are considered as 3-forms and MHD flow velocity $\mathbf{U}$ and magnetic field $\mathbf{B}$ as 2-forms. The choices for the 3-forms, $n$ and $p$, can be explained with the fact that theses quantities are densities which should be integrated over some volume to get the total mass and internal energy. Likewise, the 2-form choice for the magnetic filed is the natural choice since the magnetic field is a flux density and its surface integral should give the magnetic flux. 

On the other hand, the space of MHD flow $\mathbf{U}$ can be 1-form, 2-form or even vector field in $H^1$ space. Here, we choose 2-form rather than 1-form because this choice leads to the natural dirichlet boundary condition $\mathbf{U} \cdot \mathbf n = 0$.

With the chosen spaces of our variables, each of model equations can be solved either `strongly` or `weakly`. In this model, the momentum balance equation is solved weakly and all the others are solved strongly:
$$
\begin{equation}
\begin{aligned}
&\frac{\partial \tilde n_b}{\partial t}+ \nabla \cdot (n_{b,0} \mathbf{U}) = 0 \,,
\\
\int n_{b,0} &\frac{\partial \tilde{\mathbf{U}}}{\partial t}\cdot \mathbf{C} \textnormal{d}^3 \mathbf{x} = \int \left[(\nabla \times \mathbf{B}_0) \times \tilde{\mathbf{B}} \cdot \mathbf{C} + \tilde{\mathbf{B}} \cdot \nabla \times (\mathbf{B}_0 \times \mathbf{C}) - \nabla \tilde p \cdot \mathbf{C} \right.
\\
&\left. \qquad \qquad \qquad + \frac{A_\textnormal{h}}{A_b} \left\{ \frac{1}{\epsilon} n_\textnormal{gc} \tilde U\times \mathbf{B} \cdot \mathbf{C} - \frac{1}{\epsilon} \mathbf{J}_\textnormal{h}\times \mathbf{B} \cdot \mathbf{C} - \mathbf{M}_\textnormal{h} \cdot \nabla \times (\mathbf{B} \times \mathbf{C}) \right\}\right]\textnormal{d}^3 \mathbf{x} \,,\quad \forall \mathbf{C} \in H(\text{div})
\\
&\frac{\partial \tilde{\mathbf{B}}}{\partial t}+ \nabla \times (\mathbf{B}_0 \times \tilde{\mathbf{U}}) = 0\,,
\\
&\frac{\partial \tilde p}{\partial t}+ \nabla \cdot (p_0 \tilde{\mathbf{U}}) + (\gamma - 1)p_0\nabla \cdot \tilde{\mathbf{U}} = 0 \,,
\\
&\frac{\partial f^\text{vol}}{\partial t} + \nabla_\mathbf{x} \cdot \left(\frac{f^\text{vol}}{B^*_\parallel}(v_\parallel \mathbf{B}^* - \mathbf{b}_0 \times \mathbf{E}^*\right) + \frac{1}{\epsilon} \nabla_{v_\parallel} \left( \frac{f^\text{vol}}{B^*_\parallel} \mathbf{B}^* \cdot \mathbf{E}^*\right) = 0\,.
\end{aligned}
\end{equation}
$$
The above equation is for the normalized variables but the prime is omitted and $\mathbf{C}$ denotes the test function in $H(\text{div})$ space for the weak formulation.
### Pull-back to the logical space
In STRUPHY framwork, the model equations are decreitized on the unit cube $(0,1)^3$ with the logical coordinates $\boldsymbol{\eta} \in (0,1)^3$ and then transformed ("push-forward") to the actual problem domain $\Omega$ via the mapping $F : (0,1)^3 \rightarrow \Omega, \, \boldsymbol{\eta} \rightarrow \mathbf{x}$ which is one-to-one and differentiable where the $\mathbf{x}$ is called physical Cartesian coordinates. See [Example: Vlasov-Maxwell-Poisson discretization](https://struphy.pages.mpcdf.de/struphy/markdown/vlasov-maxwell.html#equation-eq-pic) for the discretization in STRUPHY framwork.

Now, the above equations (39) can also be written with the appropriate pull-back formulas in order to transform the model equations to the logical domain:
$$
\begin{equation}
\begin{aligned}
\frac{\partial \hat n³_b}{\partial t} &= -\hat\nabla \cdot (\frac{1}{\sqrt g}\hat n^3_{b,0} \hat{\mathbf{U}}^2) \,,
\\
\int \frac{\hat n^3_{b,0}}{\sqrt g} \frac{\partial \hat{\mathbf{U}}²}{\partial t} \cdot G \hat{\mathbf{C}}^2 \frac{1}{\sqrt g} \textnormal{d}^3 \boldsymbol{\eta} &= \int (\hat \nabla \times G \frac{1}{\sqrt g} \hat{\mathbf{B}}^2_0) \times \hat{\mathbf{B}}^2 \cdot \hat{\mathbf{C}}^2 \frac{1}{\sqrt g} \textnormal{d}^3 \boldsymbol{\eta}
\\
&+ \int \hat{\mathbf{B}}^2 \cdot G \hat \nabla \times (\hat{\mathbf{B}}^2_0 \times \frac{1}{\sqrt g} \hat{\mathbf{C}}^2) \frac{1}{\sqrt g} \textnormal{d}^3 \boldsymbol{\eta} 
\\
&+ \int  \frac{1}{\sqrt g} \hat p^3 (\hat\nabla \cdot \hat{\mathbf{C}}^2) \textnormal{d}^3 \boldsymbol{\eta}
\\
&+ \frac{A_\textnormal{h}}{A_b} \frac{1}{\epsilon} \iiint \hat f^\text{vol} (1 - \frac{\hat B^0_\parallel}{\hat B^{*0}_\parallel}) \hat{\mathbf{U}}^2 \times \frac{1}{\sqrt g} \hat{\mathbf{B}}^2_f \cdot \frac{1}{\sqrt g} \hat{\mathbf{C}}^2 \, \textnormal{d} \mu \textnormal{d} v_\parallel\textnormal{d}^3 \boldsymbol{\eta} 
\\
&- \frac{A_\textnormal{h}}{A_b} \iiint \frac{\hat f^\text{vol}}{\hat B^{*0}_\parallel} \hat v_\parallel^2 (\hat \nabla \times \hat{\mathbf{b}}^1_0)\times \frac{1}{\sqrt g} \hat{\mathbf{B}}^2_f \cdot \frac{1}{\sqrt g} \hat{\mathbf{C}}^2 \, \textnormal{d} \mu \textnormal{d} v_\parallel\textnormal{d}^3 \boldsymbol{\eta} 
\\
&- \frac{A_\textnormal{h}}{A_b} \iiint \frac{\hat f^\text{vol}}{\hat B^{*0}_\parallel} \hat \mu \left\{ G^{-1}\left(\hat{\mathbf{b}}^2_0 \times G^{-1}\hat \nabla\left[\hat{\mathbf{b}}^1_0 \cdot \frac{1}{\sqrt g} \hat{\mathbf{B}}^2_f\right]\right)\right\}\times \hat{\mathbf{B}}^2_f \cdot \frac{1}{\sqrt g} \hat{\mathbf{C}}^2\, \textnormal{d} \mu \textnormal{d} v_\parallel\textnormal{d}^3 \boldsymbol{\eta} 
\\
&+ \frac{A_\textnormal{h}}{A_b} \iiint \hat f^\text{vol} \hat \mu \hat{\mathbf{b}}^1_0 \cdot \left\{ \frac{1}{\sqrt g} \hat \nabla \times (\hat{\mathbf{B}}^2_f \times \frac{1}{\sqrt g} \hat{\mathbf{C}}^2) \right\} \, \textnormal{d} \mu \textnormal{d} v_\parallel \textnormal{d}^3 \boldsymbol{\eta} \,,
\\
\frac{\partial \hat{\mathbf{B}}²}{\partial t} &= - \hat \nabla \times (\frac{1}{\sqrt g} \hat{\mathbf{B}}^2_0 \times \hat{\mathbf{U}}^2)\,,
\\
\frac{\partial \hat p³}{\partial t} &= - \hat \nabla \cdot (\frac{1}{\sqrt g}p^3_0 \hat{\mathbf{U}}^2) - (\gamma - 1)\frac{1}{\sqrt g}p^3_0 \hat \nabla \cdot \hat{\mathbf{U}}^2 \,,
\end{aligned}
\end{equation}
$$
$$
\begin{equation*}
\frac{\partial \hat f}{\partial t} + \frac{1}{\hat B^{*0}_\parallel}\left\{v_\parallel \frac{1}{\sqrt{g}}\hat{\mathbf{B}}^{*2} - G^{-1}(\hat{\mathbf{b}}^2_0 \times G^{-1} \hat{\mathbf{E}}^{*1})\right\} \cdot \hat \nabla_{\boldsymbol{\eta}} \hat f + \frac{1}{\hat B^{*0}_\parallel}(\frac{1}{\epsilon} \frac{1}{\sqrt{g}}\hat{\mathbf{B}}^{*2} \cdot \hat{\mathbf{E}}^{*1}) \nabla_{v_\parallel} \hat f = 0\,,
\end{equation*}
$$
where 
$$
\begin{equation*}
\begin{aligned}
 \hat{\mathbf B}^{*2}(\boldsymbol{\eta}, v_\parallel) &= \hat{\mathbf{B}}^2(\boldsymbol{\eta}) + \epsilon \hat v_\parallel \hat \nabla \times \mathbf{b}^1_0(\boldsymbol{\eta}) \,,
 \\
 &= \sqrt{g(\boldsymbol{\eta})}DF^{-1}(\boldsymbol{\eta})\mathbf{B}(F(\boldsymbol{\eta})) + \epsilon v_\parallel \sqrt{g(\boldsymbol{\eta})} DF^{-1}(\boldsymbol{\eta}) \nabla \times \mathbf{b}_0(F(\boldsymbol{\eta})) \,,
 \\[2mm]
 \hat{\mathbf{E}}^{*1}(\boldsymbol{\eta}) &= - \hat{\mathbf{U}}^2(\boldsymbol{\eta}) \times \frac{1}{\sqrt{g(\boldsymbol{\eta})}} \hat{\mathbf{B}}^2(\boldsymbol{\eta}) -\epsilon \hat \mu  \hat \nabla_{\boldsymbol{\eta}} \hat B^0_\parallel(\boldsymbol{\eta}) 
 \\
 &= -DF^{-1}(\boldsymbol{\eta}) \tilde{\mathbf{U}}(F(\boldsymbol{\eta})) \times \mathbf{B}(F(\boldsymbol{\eta})) - \epsilon \mu DF^\top(\boldsymbol{\eta}) \nabla B_\parallel(F(\boldsymbol{\eta})) \,,
 \\[2mm]
 \hat{\mathbf{b}}_0^{*1}(\boldsymbol{\eta}) &= DF^{\top}(\boldsymbol{\eta}) \mathbf{b}_0(F(\boldsymbol{\eta})) \,,
 \\[2mm]
\hat B^{*0}_\parallel(\boldsymbol{\eta}, v_\parallel) &= \mathbf{b}_0(F(\boldsymbol{\eta})) \cdot \left(\mathbf{B}(F(\boldsymbol{\eta})) + \epsilon v_\parallel \nabla \times \mathbf{b}_0(F(\boldsymbol{\eta}))\right) \,,
\end{aligned}
\end{equation*}
$$
and the $\hat{\mathbf{B}}^2_f$ denotes the 2-form of $\mathbf{B} = \tilde{\mathbf{B}} + \mathbf{B}_0$ . It is important to note that the guiding-center kinetic density $f^\text{vol}(\mathbf X, v_\parallel; \mu)$ and its 0-form $f = \frac{f^\text{vol}}{B_\parallel^*}$ in continuous space can be represented as a $0$ -form and $\text{vol}$ -form of kinetic density in logical space with the two kinds of Jacobian determinant:
$$
\text{space Jacobian} : \sqrt{g}\,, \qquad  \text{velocity Jacobian} : \hat B^*_\parallel\,,
$$
$$
\begin{equation}
\hat f(\boldsymbol \eta, v_\parallel; \mu) = \frac{\hat f^\text{vol}(\boldsymbol \eta, v_\parallel; \mu)}{\sqrt{g(\boldsymbol \eta)} \, \hat B^*_\parallel(\boldsymbol \eta, v_\parallel)}\,.
\end{equation}
$$

The fourth term of the right-hand side of the momentum balance equation in (41) is the Lorentz force density coupling term $n_\textnormal{gc} \tilde{\mathbf{U}} \times \mathbf{B}$ and the $E \times B$ drift part of the current coupling term $\mathbf{J}_\textnormal{h}$:
$$
\begin{equation}
\begin{aligned}
\left(n_\textnormal{gc} \tilde{\mathbf{U}} - \mathbf{J}_{h, E \times B}\right) \times \mathbf{B} &= \iint f^\text{vol}\left[\tilde{\mathbf{U}} - \frac{1}{B^*_\parallel}\mathbf{b} \times (\tilde{\mathbf{U}} \times \mathbf{B}) \right]\times \mathbf{B} \, \textnormal{d} \mu \textnormal{d} v_\parallel \,,
\\
&= \iint f^\text{vol}\left[1 - \frac{B_\parallel}{B^*_\parallel} \right] \tilde{\mathbf{U}} \times \mathbf{B} \, \textnormal{d} \mu \textnormal{d} v_\parallel \,.
\end{aligned}
\end{equation}
$$

### Semi-discretization
Field variables are discretized according to [Geometric finite elements (FEEC)](https://struphy.pages.mpcdf.de/struphy/sections/subsections/geomFE.html#geomfe):
$$
\begin{equation}
\begin{aligned}
\hat{\mathbf{B}}^2(t, \boldsymbol{\eta}) &\approx \hat{\mathbf{B}}^2_h (t, \boldsymbol{\eta}) = \sum^3_{\mu=1} \sum_{ijk} b_{\mu, ijk}(t) \vec \Lambda^2_{\mu, ijk}(\boldsymbol{\eta}) = \sum^3_{\mu=1} \mathbf{b}^\top_\mu \vec \Lambda^2_\mu \qquad &&\in V^2_h \,,
\\
\hat{\mathbf{U}}^2(t, \boldsymbol{\eta}) &\approx \hat{\mathbf{U}}^2_h (t, \boldsymbol{\eta}) = \sum^3_{\mu=1} \sum_{ijk} u_{\mu, ijk}(t) \vec \Lambda^2_{\mu, ijk}(\boldsymbol{\eta}) = \sum^3_{\mu=1} \mathbf{u}^\top_\mu \vec \Lambda^2_\mu &&\in V^2_h \,,
\\
\hat p^3(t, \boldsymbol{\eta}) &\approx \hat p^3_h (t, \boldsymbol{\eta}) = \sum_{ijk} b_{\mu, ijk}(t) \Lambda^3_{ijk}(\boldsymbol{\eta}) = \mathbf{p}^\top \Lambda^3  &&\in V^3_h \,, 
\\
\hat n^3(t, \boldsymbol{\eta}) &\approx \hat n^3_h (t, \boldsymbol{\eta}) = \sum_{ijk} n_{\mu, ijk}(t) \Lambda^3_{ijk}(\boldsymbol{\eta}) = \mathbf{n}^\top \Lambda^3  &&\in V^3_h \,.
\end{aligned}
\end{equation}
$$
With the above discrete notation, MHD part of the equation (40) can be written as
$$
\begin{equation}
\begin{aligned}
\dot{\mathbf{n}}^\top \Lambda^3  &= - \Lambda^3 \mathbb{D} \hat \Pi^2\left[\frac{1}{\sqrt g}\hat n^3_{b,0} \vec \Lambda^2\right] \mathbf{u} \,,
\\
\sum^{3,3}_{\mu,\nu=1} \dot{\mathbf{u}}^\top_\mu \left(\int \frac{\hat n^3_{b,0}}{\sqrt g} \vec \Lambda_\mu^2 G \vec \Lambda_\nu^2 \frac{1}{\sqrt{g}} \textnormal{d}^3 \boldsymbol{\eta} \right) \mathbf{c}_\nu &= \sum^{3,3}_{\mu,\nu=1} \mathbf{b}_\mu^\top \left(\int (\hat \nabla \times G \frac{1}{\sqrt g} \hat{\mathbf{B}}^2_0) \times \vec \Lambda^2_\mu \vec \Lambda^2_\nu \frac{1}{\sqrt g} \textnormal{d}^3 \boldsymbol{\eta} \right) \mathbf{c}_\nu
\\
&+ \sum^{3,3,3,3}_{\mu,\nu,\alpha,\beta=1} \mathbf{b}^\top_\mu \int \vec \Lambda^2_\mu G \vec \Lambda^2_\alpha \frac{1}{\sqrt{g}} \textnormal{d}^3 \boldsymbol{\eta} \mathbb{C}_{\alpha,\beta} \hat \Pi^1_\beta \left[\frac{\hat{\mathbf{B}}^2_0}{\sqrt{g}} \times \vec \Lambda^2_\nu \right] \mathbf{c}_\nu
\\
&+ \sum^3_{\nu = 1} \mathbf{p}^\top \left(\int \Lambda^3 \Lambda^3 \frac{1}{\sqrt{g}}\textnormal{d}^3 \boldsymbol{\eta} \right) \mathbb{D}_\nu \mathbf{c}_\nu
\\
&+ \frac{A_\textnormal{h}}{A_b} \frac{1}{\epsilon} \sum^{3,3}_{\mu,\nu=1} \mathbf{u}_\mu\left(\iiint \hat f^\text{vol} (1 - \frac{\hat B^0_\parallel}{\hat B^{*0}_\parallel}) (\vec \Lambda_\mu^2 \times \frac{1}{\sqrt g} \hat{\mathbf{B}}^2_f) \frac{1}{\sqrt g} \vec \Lambda_\nu^2 \, \textnormal{d} \mu \textnormal{d} v_\parallel\textnormal{d}^3 \boldsymbol{\eta}  \right) \mathbf{c}_\nu
\\
&- \frac{A_\textnormal{h}}{A_b} \sum^{3,3}_{\nu=1} \left(\iiint \frac{\hat f^\text{vol}}{\hat B^{*0}_\parallel} \hat v_\parallel^2 \left\{(\hat \nabla \times \hat{\mathbf{b}}^1_0)\times \frac{1}{\sqrt g} \hat{\mathbf{B}}^2_f\right\} \frac{1}{\sqrt g}  \vec \Lambda_\nu^2  \, \textnormal{d} \mu \textnormal{d} v_\parallel\textnormal{d}^3 \boldsymbol{\eta} \right) \mathbf{c}_\nu
\\
&- \frac{A_\textnormal{h}}{A_b} \sum^{3,3,3}_{\nu,\alpha=1}  \left(\iiint \frac{\hat f^\text{vol}}{\hat B^{*0}_\parallel} \hat \mu \left\{G^{-1} \left(\hat{\mathbf{b}}^2_0 \times G^{-1}\vec{\Lambda}^1_\alpha \mathbb{G}_\alpha \hat \Pi^0\left[\hat{\mathbf{b}}^1_0 \cdot \frac{1}{\sqrt g} \hat{\mathbf{B}}^2_f\right]\right)\times \hat{\mathbf{B}}^2_f \right\}\frac{1}{\sqrt g} \vec \Lambda_\nu^2\, \textnormal{d} \mu \textnormal{d} v_\parallel\textnormal{d}^3 \boldsymbol{\eta}\right) \mathbf{c}_\nu
\\
&+ \frac{A_\textnormal{h}}{A_b} \sum^{3,3,3}_{\nu,\alpha,\beta=1}\left( \iiint \hat f^\text{vol} \hat \mu \hat{\mathbf{b}}^1_0 \frac{1}{\sqrt{g}} \vec \Lambda^2_\alpha \, \textnormal{d} \mu \textnormal{d} v_\parallel \textnormal{d}^3 \boldsymbol{\eta} \right) \mathbb{C}_{\alpha,\beta} \left(\Pi^1_\beta \left[ \frac{\hat{\mathbf{B}}^2_0}{\sqrt{g}} \times \vec \Lambda^2_\nu \right] +\Pi^1_\beta \left[ \frac{\hat{\mathbf{B}}^2}{\sqrt{g}} \times \vec \Lambda^2_\nu \right]\right) \mathbf{c}_\nu \,,
\\
\sum^{3}_\mu \dot{\mathbf{b}}_\mu^\top \vec \Lambda_\mu^2 &= -\sum^{3,3,3}_{\mu,\alpha,\nu} \vec {\Lambda^2_\mu}\mathbb{C}_{\mu,\alpha} \hat \Pi^1_\alpha \left[ \frac{\hat{\mathbf{B}}^2_0}{\sqrt{g}} \times \vec \Lambda^2_\nu \right]\mathbf{u}_\nu \,,
\\
\dot{\mathbf{p}}^\top \Lambda^3 &= \sum^{3,3}_{\alpha,\mu=1}\left(-\mathbb{D}_\alpha \hat \Pi^2_\alpha\left[\frac{\hat p^3_0}{\sqrt{g}}\vec \Lambda^2_\mu\right] -(\gamma - 1) \hat \Pi^3\left[\frac{\hat p^3_0}{\sqrt{g}} \Lambda^3\right]\mathbb{D}_\mu \right)\mathbf{u}_\mu \,,
\end{aligned}
\end{equation}
$$
where the discrete differential operators $\mathbb{G} \in \mathbb{R}^{N^1 \times N^0}, \mathbb{C} \in \mathbb{R}^{N^2 \times N^1}$ and $\mathbb{D}\in \mathbb{R}^{N^3 \times N^2}$ are $\hat \nabla, \hat \nabla \times$ and $\hat \nabla \cdot$ respectively. In STRUPHY framework, space integral of basis functions are treated as pre-defined [WeightedMassOperators](https://struphy.pages.mpcdf.de/struphy/sections/subsections/feec_classes.html#struphy.feec.mass.WeightedMassOperators) and the space projections are implemented as [BasisProjectionOperators](https://struphy.pages.mpcdf.de/struphy/sections/subsections/feec_classes.html#struphy.feec.basis_projection_ops.BasisProjectionOperators):
$$
\begin{equation}
\begin{aligned}
\mathbb{M}^2_{(\mu,ijk),(\nu,mno)} &:= \int \vec \Lambda^2_\mu G \vec \Lambda^2_\nu \frac{1}{\sqrt{g}} \textnormal{d}^3 \boldsymbol{\eta} \,,
\\
\mathbb{M}^{2,n}_{(\mu,ijk),(\nu,mno)} &:= \int \frac{\hat n^0_{b,0}}{\sqrt{g}}\vec \Lambda^2_\mu G \vec \Lambda^2_\nu \textnormal{d}^3 \boldsymbol{\eta} \,,
\\
\mathbb{M}^{2,J}_{(\mu,ijk),(\nu,mno)} &:= \int \vec \Lambda^2_\mu (\hat \nabla \times \hat{\mathbf{B}}^2_0) \times \vec \Lambda^2_\nu \frac{1}{\sqrt{g}} \textnormal{d}^3 \boldsymbol{\eta} \,,
\\
\mathbb{M}^3_{ijk,mno} &:= \int \Lambda^3_{ijk} \Lambda^3_{mno} \frac{1}{\sqrt{g}} \textnormal{d}^3 \boldsymbol{\eta} \,,
\\
\mathcal{Q}_{(\mu,ijk),(\nu,mno)} &:= \hat \Pi^2_{(\mu,ijk)} \left[\frac{\hat n^3_{b,0}}{\sqrt{g}} \vec \Lambda^2_{(\nu,mno)} \right] \,,
\\
\mathcal{T}_{(\mu,ijk),(\nu,mno)} &:= \hat \Pi^1_{(\mu,ijk)} \left[\frac{\hat{\mathbf{B}}^2_0}{\sqrt{g}} \times \vec \Lambda^2_{(\nu,mno)} \right] \,,
\\
\mathcal{T}^B_{(\mu,ijk),(\nu,mno)} &:= \hat \Pi^1_{(\mu,ijk)} \left[\frac{\hat{\mathbf{B}}^2}{\sqrt{g}} \times \vec \Lambda^2_{(\nu,mno)} \right] \,,
\\
\mathcal{P}^b_{ijk,(\nu,mno)} &:= \hat \Pi^0_{ijk} \left[\hat{\mathbf{b}}^1_0\cdot \frac{\vec \Lambda^2_{(\nu,mno)}}{\sqrt{g}} \right] \,,
\end{aligned}
\end{equation}
$$
This leads to the equations with much more compact notation
$$
\begin{equation}
\begin{aligned}
\dot{\mathbf{n}} &= - \mathbb{D}\mathcal{Q}^2\mathbf{u}
\\
\dot{\mathbf{u}}^\top \mathbb{M}^{2,n} \mathbf{c} &= \mathbf{b}^\top \mathbb{M}^{2,J} \mathbf{c} + \mathbf{b}^\top \mathbb{M}^2 \mathbb{C} \mathcal{T} \mathbf{c} + \mathbf{p}^\top \mathbb{M}^3 \mathbb{D} \mathbf{c}
\\
&+ \frac{A_\textnormal{h}}{A_b} \frac{1}{\epsilon} \sum^{3,3}_{\mu,\nu=1} \mathbf{u}_\mu\left(\iiint \hat f^\text{vol} (1 - \frac{\hat B^0_\parallel}{\hat B^{*0}_\parallel}) (\vec \Lambda_\mu^2 \times \frac{1}{\sqrt g} \hat{\mathbf{B}}^2_f) \frac{1}{\sqrt g} \vec \Lambda_\nu^2\, \textnormal{d} \mu \textnormal{d} v_\parallel\textnormal{d}^3 \boldsymbol{\eta}  \right) \mathbf{c}_\nu
\\
&- \frac{A_\textnormal{h}}{A_b} \sum^{3,3}_{\mu,\nu=1} \left(\iiint \frac{\hat f^\text{vol}}{\hat B^{*0}_\parallel} \hat v_\parallel^2 \left\{(\hat \nabla \times \hat{\mathbf{b}}^1_0)\times \frac{1}{\sqrt g} \hat{\mathbf{B}}^2_f\right\} \frac{1}{\sqrt g}  \vec \Lambda_\nu^2 \, \textnormal{d} \mu \textnormal{d} v_\parallel\textnormal{d}^3 \boldsymbol{\eta} \right) \mathbf{c}_\nu
\\
&- \frac{A_\textnormal{h}}{A_b} \sum^{3,3,3,3}_{\mu,\nu,\alpha,\beta=1}  \left(\iiint \frac{\hat f^\text{vol}}{\hat B^{*0}_\parallel} \hat \mu \left\{G^{-1} \left(\hat{\mathbf{b}}^2_0 \times G^{-1} (\hat \nabla \hat B^1_{\parallel,0} + \vec{\Lambda}^1_\alpha \mathbb{G}_\alpha \mathcal{P}^b_\beta \mathbf{b}_\beta)\right)\times \hat{\mathbf{B}}^2_f \right\}\frac{1}{\sqrt g} \vec \Lambda_\nu^2\, \textnormal{d} \mu \textnormal{d} v_\parallel\textnormal{d}^3 \boldsymbol{\eta}\right) \mathbf{c}_\nu
\\
&+ \frac{A_\textnormal{h}}{A_b} \sum^{3,3,3}_{\nu,\alpha,\beta=1}\left( \iiint \hat f^\text{vol} \hat \mu \hat{\mathbf{b}}^1_0 \frac{1}{\sqrt{g}} \vec \Lambda^2_\alpha \, \textnormal{d} \mu \textnormal{d} v_\parallel \textnormal{d}^3 \boldsymbol{\eta} \right) \mathbb{C}_{\alpha,\beta} (\mathcal{T}_{\beta, \nu} + \mathcal{T}^B_{\beta, \nu}) \mathbf{c}_\nu \,,
\\
\dot{\mathbf{b}} &= -\mathbb{C}\mathcal{T}\mathbf{u} \,,
\\
\dot{\mathbf{p}} &= \left(-\mathbb{D}\mathcal{S} - (\gamma-1)\mathcal{K}\mathbb{D}\right)\mathbf{u} \,.
\end{aligned}
\end{equation}
$$
The kinetic equation (last line equation (40)) is discretized by means of [Particle-in-cell methods(PIC)](https://struphy.pages.mpcdf.de/struphy/sections/subsections/pic.html):
$$
\begin{equation}
\hat f^\text{vol}(t, \boldsymbol{\eta}, v_\parallel; \mu) \approx \hat f_h^\text{vol}(t, \boldsymbol{\eta}, v_\parallel; \mu) = \frac{1}{N} \omega_p \delta(\boldsymbol{\eta} - \boldsymbol{\eta}_p(t)) \delta(v_\parallel - v_{\parallel,p}) \,,
\end{equation}
$$
where $\boldsymbol{\eta}_p(t)$ and $v_{\parallel,p}(t)$ satisfy the characteristics of the kinetic transport equation (last line equation (41)),
$$
\begin{equation}
\begin{aligned}
\dot{\boldsymbol{\eta}}_p &=  \frac{1}{\hat B^{*0}_\parallel(\boldsymbol{\eta}_p,v_{\parallel,p})}\left\{\hat v_{\parallel,p} \frac{1}{\sqrt{g(\boldsymbol{\eta}_p)}} \hat{\mathbf{B}}^{*2}(\boldsymbol{\eta}_p, v_{\parallel,p}) - G^{-1}(\boldsymbol{\eta}_p) \left(\hat{\mathbf{b}}^2_0(\boldsymbol{\eta}_p) \times G^{-1}(\boldsymbol{\eta}_p) \hat{\mathbf{E}}^{*1}(\boldsymbol{\eta}_p) \right)\right\} \,,
\\
\dot {\hat{v}}_{\parallel,p} &= \frac{1}{\epsilon} \frac{1}{\hat B^{*0}_\parallel(\boldsymbol{\eta}_p,v_{\parallel,p})} \frac{1}{\sqrt{g(\boldsymbol{\eta}_p)}}\hat{\mathbf{B}}^{*2}(\boldsymbol{\eta}_p,v_{\parallel,p}) \cdot \hat{\mathbf{E}}^{*1}(\boldsymbol{\eta}_p) \,.
\end{aligned}
\end{equation}
$$
In the model, there are 4 coupling terms:
$$
\begin{equation}
\begin{aligned}
\mathcal{D}\mathcal{C}_{(\mu,ijk),(\nu,mno)} &= \frac{A_\textnormal{h}}{A_b} \frac{1}{\epsilon} \iiint \hat f^\text{vol} \left(1 - \frac{\hat B^0_\parallel}{\hat B^{*0}_\parallel}\right) (\vec \Lambda_{\mu,ijk}^2 \times \frac{1}{\sqrt g} \hat{\mathbf{B}}^2_f) \frac{1}{\sqrt g} \vec \Lambda_{\nu,mno}^2\, \textnormal{d} \mu \textnormal{d} v_\parallel\textnormal{d}^3 \boldsymbol{\eta} \,,
\\
&\approx \frac{A_\textnormal{h}}{A_b} \frac{1}{\epsilon} \frac{1}{N_p} \sum^{N_p-1}_{p=0} \omega_p \left(1 - \frac{\hat B^0_\parallel(\boldsymbol{\eta}_p)}{\hat B^{*0}_\parallel(\boldsymbol{\eta}_p, v_{\parallel,p})}\right) \left(\vec \Lambda_{\mu,ijk}^2(\boldsymbol{\eta}_p) \times \frac{1}{\sqrt{g(\boldsymbol{\eta}_p)}} \hat{\mathbf{B}}^2_f(\boldsymbol{\eta}_p)\right) \frac{1}{\sqrt{g(\boldsymbol{\eta}_p)}} \vec \Lambda_{\nu,mno}^2(\boldsymbol{\eta}_p) \,,
\\
&= \frac{A_\textnormal{h}}{A_b} \frac{1}{\epsilon} \mathbb{L}²({\bar{B}}^\times_f)^\top \frac{1}{\bar g} \bar{W} \left(\bar 1- \frac{\bar B⁰_\parallel}{\bar B^{*0}_\parallel}\right) (\mathbb{L}²)^\top \,,
\\
\mathcal{J}\mathcal{C}¹_{\mu,ijk} &= - \frac{A_\textnormal{h}}{A_b} \iiint \frac{\hat f^\text{vol}}{\hat B^{*0}_\parallel} \hat v_\parallel^2 \left\{(\hat \nabla \times \hat{\mathbf{b}}^1_0)\times \frac{1}{\sqrt g} \hat{\mathbf{B}}^2_f\right\} \frac{1}{\sqrt g}  \vec \Lambda_{\mu,ijk}^2\, \textnormal{d} \mu \textnormal{d} v_\parallel\textnormal{d}^3 \boldsymbol{\eta} \,,
\\
&\approx - \frac{A_\textnormal{h}}{A_b} \frac{1}{N_p}\sum^{N_p-1}_{p=0}\omega_p \frac{\hat v_{\parallel,p}²}{\hat B_\parallel^{*0}(\boldsymbol{\eta}_p, v_{\parallel,p})} \left((\hat \nabla \times \hat{\mathbf{b}}¹_0)(\boldsymbol{\eta}_p) \times \frac{1}{\sqrt{g(\boldsymbol{\eta}_p)}} \hat{\mathbf{B}}^2_f(\boldsymbol{\eta}_p)\right) \frac{1}{\sqrt{g(\boldsymbol{\eta}_p)}} \vec \Lambda_{\mu,ijk}^2 \,,
\\
&= - \frac{A_\textnormal{h}}{A_b} \bar{b}^{\nabla \times}_0 (\bar{B}^\times_f)^\top \frac{1}{\bar B^{*0}_\parallel} \bar{V}_\parallel \bar{V}_\parallel \bar{W} \frac{1}{\bar g} (\mathbb{L}²)^\top \,,
\\
&= - \frac{A_\textnormal{h}}{A_b} V_\parallel W \frac{1}{\bar B^{*0}_\parallel} \left\{\bar{b}^{\nabla \times}_0 (\bar{B}^\times_f)^\top \bar{V}_\parallel \frac{1}{\bar{\sqrt{g}}}\right\}\, \cdot_\text{vector} \left\{\frac{1}{\bar{\sqrt{g}}}(\mathbb{L}²)^\top\right\}\,,
\\
\mathcal{J}\mathcal{C}²_{\mu,ijk} &= -\frac{A_\textnormal{h}}{A_b} \iiint \frac{\hat f^\text{vol}}{\hat B^{*0}_\parallel} \hat \mu \left\{G^{-1} \left(\hat{\mathbf{b}}^2_0 \times G^{-1}(\hat \nabla \hat B^1_{\parallel,0} + \vec{\Lambda}^1 \mathbb{G} \mathcal{P}^b \mathbf{b})\right)\times \hat{\mathbf{B}}^2_f \right\}\frac{1}{\sqrt g} \vec \Lambda_{\mu,ijk}^2\, \textnormal{d} \mu \textnormal{d} v_\parallel\textnormal{d}^3 \boldsymbol{\eta} \,,
\\
&\approx \frac{A_\textnormal{h}}{A_b} \frac{1}{N_p}\sum^{N_p-1}_{p=0}\omega_p \frac{\hat \mu_p}{\hat B_\parallel^{*0}(\boldsymbol{\eta}_p, v_{\parallel,p})} \left[\hat{\mathbf{B}}^2_f(\boldsymbol{\eta}_p) \times G^{-1}(\boldsymbol{\eta}_p)\left\{\hat{\mathbf{b}}²_0(\boldsymbol{\eta}_p) \times G^{-1}(\boldsymbol{\eta}_p)(\hat \nabla \hat B^1_{\parallel,0}(\boldsymbol{\eta}_p) + \vec{\Lambda}^1(\boldsymbol{\eta}_p) \mathbb{G} \mathcal{P}^b \mathbf{b})\right\} \right]\frac{1}{\sqrt{g(\boldsymbol{\eta}_p)}} \vec \Lambda_{\mu,ijk}^2 \,,
\\
&= \frac{A_\textnormal{h}}{A_b} \bar{W}\bar{M}\frac{1}{\bar B^{*0}_\parallel}\bar{B}^\times_f \bar{G}^{-1} \bar{b}^\times_0 \bar{G}^{-1}  \frac{1}{\bar{\sqrt{g}}}\overline{\nabla B}_\parallel (\mathbb{L}²)^\top \,,
\\
&= \frac{A_\textnormal{h}}{A_b} M W \frac{1}{\bar B^{*0}_\parallel} \left\{\bar{B}^\times_f \bar{G}^{-1} \bar{b}^\times_0 \bar{G}^{-1}  \overline{\nabla B}_\parallel \right\}\, \cdot_\text{vector} \left\{\frac{1}{\bar{\sqrt{g}}}(\mathbb{L}²)^\top\right\}\,,
\\
\mathcal{M}\mathcal{C}_{\mu,ijk} &= \frac{A_\textnormal{h}}{A_b} \iiint \hat f^\text{vol} \hat \mu \hat{\mathbf{b}}^1_0 \frac{1}{\sqrt{g}} \vec \Lambda^2_{\mu,ijk}\, \textnormal{d} \mu \textnormal{d} v_\parallel \textnormal{d}^3 \boldsymbol{\eta} \,,
\\
&\approx \frac{A_\textnormal{h}}{A_b}\frac{1}{N_p} \sum^{N_p-1}_{p=0}\omega_p \mu_p \hat{\mathbf{b}}^1_0(\boldsymbol{\eta}_p) \frac{1}{\sqrt{g(\boldsymbol{\eta}_p)}} \vec \Lambda_{\mu,ijk}^2 \,,
\\
&= \frac{A_\textnormal{h}}{A_b}\bar{W}\bar{M}\frac{1}{\bar{\sqrt{g}}} \bar{b}^1_0 (\mathbb{L}²)^\top \,.
\\
&= \frac{A_\textnormal{h}}{A_b}  M W \, \bar{b}¹_0\cdot_\text{vector} \left\{\frac{1}{\bar{\sqrt{g}}}(\mathbb{L}²)^\top\right\} \,,
\end{aligned}
\end{equation}
$$
where the `vector` dot notation $\cdot_\text{vector}$ stands for the contraction in $(\mu = 1,2,3)$ i.e. 
$$
\begin{equation}
\left[ V_\mu \in \mathbb{R}^{3 \times N_p}\right] \cdot_\text{vector} \left[W_\mu \in \mathbb{R}^{3 \times N_p}\right] \rightarrow  \sum³_{\mu=1} V_\mu W_\mu \in \mathbb{R}^{N_p} \,.
\end{equation}
$$
Here, we define stacked matrices in order to write the particle accumulation compactly:
$$
\begin{equation}
\begin{aligned}
\boldsymbol{\eta} &:= (\eta_{1,0}, \dots, \eta_{1,N_p},\eta_{2,0}, \dots, \eta_{2,N_p},\eta_{3,0}, \dots, \eta_{3,N_p}) \qquad &&\in \mathbb{R}^{3 \times N_p} \,,
\\
V_\parallel &:= (v_{\parallel,0}, \dots v_{\parallel,N_p}) &&\in \mathbb{R}^{N_p} \,,
\\
\Mu &:= (\mu_{0}, \dots \mu_{N_p}) &&\in \mathbb{R}^{N_p} \,,
\\
\bar{V}_\parallel &:= \text{diag}(V_\parallel) \otimes I_{3 \times 3} &&\in \mathbb{R}^{(3\times N_p) \times (3\times N_p)}
\\
\bar{M} &:= \text{diag}(\Mu)) \otimes I_{3 \times 3} &&\in \mathbb{R}^{(3\times N_p) \times (3\times N_p)}
\\
\mathbb{L}² &:= \begin{bmatrix}
\mathbb{L}²_1(\boldsymbol{\eta}) & 0 & 0 \\
0 & \mathbb{L}²_2(\boldsymbol{\eta}) & 0 \\
0 & 0 & \mathbb{L}²_3(\boldsymbol{\eta}) \\
\end{bmatrix} \,, \quad (\mathbb{L}²_\mu)_{(ijk,p)} := \vec \Lambda²_{\mu,ijk}(\boldsymbol{\eta}_p) \qquad &&\in \mathbb{R}^{N² \times (3 \times N_p)} \,,
\\
\bar{W} &:= W \otimes I_{3 \times 3}\,, \quad W := \text{diag}\left(\frac{\omega_0}{N_p}, \cdots, \frac{\omega_{N_p}}{N_p}\right) && \in \mathbb{R}^{(3\times N_p) \times (3\times N_p)}
\\
\frac{1}{\bar g} &:= \text{diag}\left(\frac{1}{g(\boldsymbol{\eta}_0)}, \cdots, \frac{1}{g(\boldsymbol{\eta}_{N_p})}\right) \otimes I_{3 \times 3} &&\in \mathbb{R}^{(3\times N_p) \times (3\times N_p)} \,,
\\
\frac{1}{\bar{\sqrt{g}}} &:= \text{diag}\left(\frac{1}{\sqrt{g(\boldsymbol{\eta}_0)}}, \cdots, \frac{1}{\sqrt{g(\boldsymbol{\eta}_{N_p})}}\right) \otimes I_{3 \times 3} &&\in \mathbb{R}^{(3\times N_p) \times (3\times N_p)} \,,
\\
\frac{1}{\bar B^{*0}_\parallel} &:= \text{diag}\left(\frac{1}{\hat B^{*0}_\parallel(\boldsymbol{\eta}_0)}, \cdots, \frac{1}{\hat B^{*0}_\parallel(\boldsymbol{\eta}_{N_p})}\right) \otimes I_{3 \times 3} &&\in \mathbb{R}^{(3\times N_p) \times (3\times N_p)} \,,
\\
\left(\bar 1- \frac{\bar B⁰_\parallel}{\bar B^{*0}_\parallel}\right) &:= \text{diag}\left(1- \frac{\hat B⁰_\parallel(\boldsymbol{\eta}_0)}{\hat B^{*0}_\parallel(\boldsymbol{\eta}_0)}, \cdots, 1- \frac{\hat B⁰_\parallel(\boldsymbol{\eta}_{N_p})}{\hat B^{*0}_\parallel(\boldsymbol{\eta}_{N_p})}\right) \otimes I_{3 \times 3} &&\in \mathbb{R}^{(3\times N_p) \times (3\times N_p)} \,,
\\
\bar{G}^{-1} &:= [\bar{G}^{-1}_{\mu\nu}]_{\mu,\nu=1,2,3,} \,, \quad \bar{G}^{-1}_{\mu\nu} := \text{diag}(G^{-1}_{\mu\nu}(\boldsymbol \eta_0)\,, \cdots, G^{-1}_{\mu\nu}(\boldsymbol \eta_{N_p})) &&\in \mathbb{R}^{(3\times N_p) \times (3\times N_p)} \,,
\\
\bar{B}^\times_f &:= \left[ \bar{B}^\times_{f,\mu\nu} \right]_{\mu,\nu = 1,2,3} \,, \quad \bar{B}^\times_{f,\mu\nu} := \epsilon_{\mu \alpha \nu} \text{diag}\left((\mathbf{b}_\alpha + \mathbf{b}_{0,\alpha})^\top \mathbb{L}²_\alpha\right)&&\in \mathbb{R}^{(3\times N_p) \times (3\times N_p)} \,,
\\
\bar{b}^\times_f &:= \left[ \bar{b}^\times_{0,\mu\nu} \right]_{\mu,\nu = 1,2,3} \,, \quad \bar{b}^\times_{0,\mu\nu} := \epsilon_{\mu \alpha \nu} \text{diag}\left( \hat{\mathbf{b}}²_{0,\alpha} (\boldsymbol{\eta}_0), \cdots, \hat{\mathbf{b}}²_{0,\alpha} (\boldsymbol{\eta}_{N_p})\right)&&\in \mathbb{R}^{(3\times N_p) \times (3\times N_p)} \,,
\\
\bar{b}^{\nabla \times}_0 &:= \left[\bar{b}^{\nabla \times}_{0,\mu}\right]_{\mu=1,2,3}\,, \quad \bar{b}^{\nabla \times}_{0,\mu} := \left((\hat \nabla \times \hat{\mathbf{b}}¹_0)_\mu(\boldsymbol{\eta}_0), \cdots, (\hat \nabla \times \hat{\mathbf{b}}¹_0)_\mu(\boldsymbol{\eta}_{N_p})\right) &&\in \mathbb{R}^{3 \times N_p} \,,
\\
\bar{b}^{1}_0 &:= \left[\bar{b}^{1}_{0,\mu}\right]_{\mu=1,2,3}\,, \quad \bar{b}^{1}_{0,\mu} := \left(\hat{\mathbf{b}}¹_{0,\mu}(\boldsymbol{\eta}_0), \cdots, \hat{\mathbf{b}}¹_{0,\mu}(\boldsymbol{\eta}_{N_p})\right) &&\in \mathbb{R}^{3 \times N_p} \,,
\\
\bar{B}_f &:= \left[\bar{B}_{f,\mu}\right]_{\mu=1,2,3}\,, \quad \bar{B}_{f,\mu} := (\mathbf{b}_\mu + \mathbf{b}_{0,\mu})^\top \mathbb{L}^2_\mu &&\in \mathbb{R}^{3 \times N_p} \,,
\\
\overline{\nabla B}_\parallel &:= \left[\overline{\nabla B}_{\parallel,\mu}\right]_{\mu=1,2,3}\,, \quad \overline{\nabla B}_{\parallel,\mu} := \left((\hat\nabla \hat B¹_{\parallel,0})_\mu(\boldsymbol{\eta}_0) + (\vec \Lambda^1_\mu)^\top(\boldsymbol{\eta}_0)\mathbb{G}\mathcal{P}^b\mathbf{b}, \cdots, (\hat\nabla \hat B¹_{\parallel,0})_\mu(\boldsymbol{\eta}_{N_p}) + (\vec \Lambda^1_\mu)^\top(\boldsymbol{\eta}_{N_p})\mathbb{G}\mathcal{P}^b\mathbf{b}\right) &&\in \mathbb{R}^{3 \times N_p} \,,
\end{aligned}
\end{equation}
$$
where $\mathbf{b}_0 = (\mathbf{b}_{0,1}, \mathbf{b}_{0,2}, \mathbf{b}_{0,3})$ is the FE coefficient of the projected equilibrium magnetic field.

With the compact notation of the coupling terms, the equation (46) and (48) can be written with the discretized variables:
$$
\begin{equation}
\begin{aligned}
\dot{\mathbf{n}} &= - \mathbb{D}\mathcal{Q}^2\mathbf{u}
\\
\dot{\mathbf{u}}^\top \mathbb{M}^{2,n} \mathbf{c} &= \mathbf{b}^\top \mathbb{M}^{2,J} \mathbf{c} + \mathbf{b}^\top \mathbb{M}^2 \mathbb{C} \mathcal{T} \mathbf{c} + \mathbf{p}^\top \mathbb{M}^3 \mathbb{D} \mathbf{c} + \mathbf{u}^\top \mathcal{D}\mathcal{C} \mathbf{c} + \mathcal{J}\mathcal{C}^1 \mathbf{c} + \mathcal{J}\mathcal{C}² \mathbf{c} + \mathcal{M}\mathcal{C}  \mathbb{C}(\mathcal{T} + \mathcal{T}^B) \mathbf{c} \,,
\\
\dot{\mathbf{b}} &= -\mathbb{C}\mathcal{T}\mathbf{u} \,,
\\
\dot{\mathbf{p}} &= \left(-\mathbb{D}\mathcal{S} - (\gamma-1)\mathcal{K}\mathbb{D}\right)\mathbf{u} \,,
\\[2mm]
\dot{\boldsymbol{\eta}} &=\left[ \frac{1}{\bar B^{*0}_\parallel} \frac{1}{\bar{\sqrt{g}}} \bar{B}_f + \epsilon  \frac{1}{\bar B^{*0}_\parallel}\bar{V}_\parallel \frac{1}{\bar{\sqrt{g}}}\bar{b}^{\nabla\times}_0 \right] \cdot_\text{scalar}V_\parallel
\\
&\quad + \epsilon \bar{M}\frac{1}{\bar B^{*0}_\parallel}\bar{G}^{-1}  \bar{b}^\times_0\bar{G}^{-1} \overline{\nabla B}_\parallel 
\\
&\quad - \frac{1}{\bar B^{*0}_\parallel}\frac{1}{\bar{\sqrt{g}}}\bar{G}^{-1}  \bar{b}^\times_0 \bar{G}^{-1}  \bar{B}^\times_f \bar{G}^{-1}  (\mathbb{L}^2)^\top \mathbf{u} \,,
\\
\dot V_\parallel &=  -\left[ \frac{1}{\bar B^{*0}_\parallel}\frac{1}{\bar{\sqrt{g}}}\bar{B}_f + \epsilon  \frac{1}{\bar B^{*0}_\parallel}\bar{V}_\parallel \frac{1}{\bar{\sqrt{g}}}\bar{b}^{\nabla \times}_0 \right] \cdot_\text{vector} \left( \bar{M} \overline{\nabla B}_\parallel\right) 
\\
&\quad + \frac{1}{\bar B^{*0}_\parallel} \left\{\bar{b}^{\nabla \times}_0 (\bar{B}^\times_f)^\top \bar{V}_\parallel \frac{1}{\bar{\sqrt{g}}} \right\} \cdot_\text{vector} \left\{\frac{1}{\bar{\sqrt{g}}} (\mathbb{L}^2)^\top\mathbf{u}\right\}\,,
\end{aligned}
\end{equation}
$$
where the `scalar` dot is defined as
$$
\begin{equation}
\left[ V_\mu \in \mathbb{R}^{3 \times N_p}\right] \cdot_\text{scalar} \left[W \in \mathbb{R}^{N_p}\right] \rightarrow  V_\mu W \in \mathbb{R}^{3 \times N_p} \,.
\end{equation}
$$
### Hamiltonian structure
Since the system of the MHD-Driftkinetic Hybrid model is originally derived from the variational principles, there is a corresponding non-canonical Hamiltonian structure with the conserved total energy:
$$
\begin{equation}
\mathcal{H}( \mathbf{U}, \mathbf{B}, p, f^\text{vol}) = \int \frac{\rho}{2} \mathbf{U}² + \frac{1}{2\mu} \mathbf{B}² + \frac{p}{\gamma - 1} \iint f^\text{vol}\frac{m_\textnormal{h}}{2}v²_\parallel + f^\text{vol}\mu B_\parallel \textnormal{d} v_\parallel \textnormal{d} \mu \textnormal{d}³ \mathbf{x} \,.
\end{equation}
$$
After linearization, the total energy is, however, no longer conserved, so then we define the perturbed energy 
$$
\begin{equation}
\tilde{\mathcal{H}}(\tilde{\mathbf{U}}, \tilde{\mathbf{B}}, \tilde p, f^\text{vol}) = \int \frac{\rho}{2} \tilde{\mathbf{U}}² + \frac{1}{2\mu} \tilde{\mathbf{B}}² + \frac{\tilde p}{\gamma - 1} + \iint f^\text{vol}\frac{m_\textnormal{h}}{2}v²_\parallel + f^\text{vol}\mu (B_0 + \tilde B_\parallel) \textnormal{d} v_\parallel \textnormal{d} \mu \textnormal{d}³ \mathbf{x} 
\end{equation}
$$
which evolves in time (see the equation (32)).

In the discretized space, the energy can be written
$$
\begin{equation}
\begin{aligned}
\tilde{\mathcal{H}}_h(\mathbf{u}, \mathbf{b}, \mathbf{p}, \boldsymbol{\eta}, V_\parallel) &= \frac{1}{2} \mathbf{u}^\top \mathbb{M}^{2,n} \mathbf{u} + \frac{1}{2} \mathbf{b}^\top \mathbb{M}²\mathbf{b} + \frac{1}{\gamma - 1}\mathbf{p}^\top \mathbf{1}³ 
\\
&+ \frac{A_\textnormal{h}}{A_b} \frac{1}{2}V_\parallel W V_\parallel + \frac{A_\textnormal{h}}{A_b} \bar M \bar W \left(\bar B_0(\boldsymbol{\eta}) + \bar{b}^1_0 \cdot_\text{vector} \frac{1}{\bar{\sqrt{g}}}(\mathbb{L}^2)^\top \mathbf{b}\right) \,,
\end{aligned}
\end{equation}
$$
where $\mathbf{1}³ := (1, \cdots, 1) \in \mathbb{R}^{N³}$ is a vector filled with ones and $\bar B_0$ and $\mathbb{L}⁰$ are stacked matrices for the particle accumulation
$$
\begin{equation}
\begin{aligned}
\mathbb{L}⁰ &:=[\mathbb{L}⁰_{ijk,p}]\,, \quad \mathbb{L}⁰_{ijk,p} = \Lambda⁰_{ijk}(\boldsymbol{\eta}_p) \qquad &\in \mathbb{R}^{N⁰ \times N_p} \,,
\\
\bar B_0 &:= \text{diag} \left(\hat B⁰_0(\boldsymbol{\eta}_0), \, \cdots,\, \hat B⁰_0(\boldsymbol{\eta}_p)\right) &\in \mathbb{R}^{N_p \times N_p} \,.
\end{aligned}
\end{equation}
$$
We can define the vector of unknowns $\mathbf{Z}:=(\mathbf{u}, \mathbf{b}, \mathbf{p}, \boldsymbol{\eta}, V_\parallel) \in \mathbb{R}^{N² + N² + N³ + (3\times N_p) + N_p}$ and then write the discretized system (equation (52)) in the following compact form:
$$
\begin{equation}
\begin{aligned}
\frac{\textnormal{d} \mathbf{Z}}{\textnormal{d} t} &= \mathbb{J} \nabla_\mathbf{Z} \tilde{\mathcal{H}}_h + \mathbb{K}\mathbf{Z} \,,
\\
&= \overbrace{\begin{bmatrix}
J_{11} & J_{12} & 0 & J_{14} & J_{15} \\
-J_{12}^\top & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 \\
-J_{14}^\top & 0 & 0 & J_{44} & J_{45} \\
-J_{15}^\top & 0 & 0 & -J_{45}^\top & 0 \\
\end{bmatrix}}^{:=\mathbb{J}}
\overbrace{\begin{bmatrix}
\mathbb{M}^{2,n}\mathbf{u} \\
\mathbb{M}² \mathbf{b} + \frac{A_\textnormal{h}}{A_b}M\bar W \bar{b}^1_0 \cdot_\text{vector} \frac{1}{\bar{\sqrt{g}}}(\mathbb{L}^2)^\top\\
\frac{1}{\gamma - 1} \mathbf{1}³ \\
\frac{A_\textnormal{h}}{A_b}\bar{M}\bar{W}\overline{\nabla B}_\parallel \\
\frac{A_\textnormal{h}}{A_b} W V_\parallel
\end{bmatrix}}^{:=\nabla_\mathbf{Z} \tilde{\mathcal{H}}_h}
\\
\\
&+ \underbrace{\begin{bmatrix}
0 & (\mathbb{M}^{2,n})^{-1}\mathbb{M}^{2,J} & (\mathbb{M}^{2,n})^{-1}\mathbb{D}^\top \mathbb{M}^3 & (\mathbb{M}^{2,n})^{-1}(\mathcal{T}^B)^\top \mathbb{C}^\top (\mathcal{M}\mathcal{C})^\top \boldsymbol{\eta}^{-1} & 0 \\
0 & 0 & 0 & 0 & 0 \\
-\mathcal{S}^\top \mathbb{D}^\top- (\gamma-1)\mathbb{D}^\top \mathcal{K}^\top & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 \\
\end{bmatrix}}_{:=\mathbb{K}}
\underbrace{\begin{bmatrix}
\mathbf{u} \\
\mathbf{b}\\
\mathbf{p}\\
\boldsymbol{\eta}\\
V_\parallel
\end{bmatrix}}_{=\mathbf{Z}} \,,
\end{aligned}
\end{equation}
$$
where the components of $\mathbb{J}$ are given by
$$
\begin{equation}
\begin{aligned}
J_{11}(\mathbf{b}, \boldsymbol{\eta}) &:= -\frac{A_\textnormal{h}}{A_b} \frac{1}{\epsilon} \mathbb{L}²{\bar{B}}^\times_f \frac{1}{\bar g} \bar{W} \left(\bar 1- \frac{\bar B⁰_\parallel}{\bar B^{*0}_\parallel}\right) (\mathbb{L}²)^\top(\mathbb{M}^{2,n})^{-1} \,,
\\
J_{12} &:= (\mathbb{M}^{2,n})^{-1} \mathcal{T}^\top \mathbb{C}^\top \,,
\\
J_{14}(\mathbf{b}, \boldsymbol{\eta}) &:= (\mathbb{M}^{2,n})^{-1} \mathbb{L}² \frac{1}{\bar{\sqrt{g}}} \frac{1}{\bar B^{*0}_\parallel} \bar{B}^\times_f \bar{G}^{-1} \bar{b}^\times_0 \bar{G}^{-1}  \,,
\\
J_{15}(\mathbf{b}, \boldsymbol{\eta}, V_\parallel) &:= - (\mathbb{M}^{2,n})^{-1} \left\{\frac{1}{\bar{\sqrt{g}}}(\mathbb{L}²)^\top\right\}\cdot_\text{vector} \left\{\bar{b}^{\nabla \times}_0 (\bar{B}^\times_f)^\top \bar{V}_\parallel \frac{1}{\bar{\sqrt{g}}}\right\} \frac{1}{\bar B^{*0}_\parallel}\,,
\\
J_{44}(\mathbf{b}, \boldsymbol{\eta}) &:=\frac{A_b}{A_\textnormal{h}} \bar{W}^{-1} \frac{1}{\bar B^{*0}_\parallel}\bar{G}^{-1}  \bar{b}^\times_0\bar{G}^{-1}  \,,
\\
J_{45}(\mathbf{b}, \boldsymbol{\eta}, V_\parallel) &:= \frac{A_b}{A_\textnormal{h}} W^{-1} \frac{1}{\bar B^{*0}_\parallel} \left[\frac{1}{\bar{\sqrt{g}}} \bar{B}_f + \epsilon\bar{V}_\parallel \frac{1}{\bar{\sqrt{g}}}\bar{b}^{\nabla\times}_0 \right] \cdot \,.
\\
\end{aligned}
\end{equation}
$$
From the above equation, it is noted that the model equations of the LinearMHD-Driftkinetic system can be written in a system of ordinary differential equations in time which can be expressed as the sum of a part with an anti-symmetric matrix $\mathbb{J}$ and an additional part with a matrix $\mathbb{K}$. The anti-symmetriy $\mathbb{J}^\top = -\mathbb{J}$ ensures the conservation of the Hamiltonain since
$$
\begin{equation}
\frac{\textnormal{d}}{\textnormal{d} t} \tilde{\mathcal{H}}_h(\mathbf{Z}(t)) = (\nabla_\mathbf{Z} \tilde{\mathcal{H}}_h)^\top \frac{\textnormal{d} \mathbf{Z}}{\textnormal{d} t} = (\nabla_\mathbf{Z} \tilde{\mathcal{H}}_h)^\top \mathbb{J} \nabla_\mathbf{Z} \tilde{\mathcal{H}}_h = - (\nabla_\mathbf{Z} \tilde{\mathcal{H}}_h)^\top \mathbb{J} \nabla_\mathbf{Z} \tilde{\mathcal{H}}_h  = 0 
\end{equation} \,.
$$

### Time discretization
In terms of time discretization, our aim is to preserve the skew symmetry of $\mathbb{J}$. This is called `Poisson splitting`:
$$
\mathbb{J} = \mathbb{J}_\text{Shear Alfvén} + \mathbb{J}_\text{CC-Denisty} + \mathbb{J}_\text{CC-GradB} + \mathbb{J}_\text{CC-Curlb} + \mathbb{J}_{DK-GradB} + + \mathbb{J}_{DK-Bstar}\,,
$$
with
$$
\begin{equation}
\begin{aligned}
\mathbb{J}_\text{Shear Alfvén} &:= \begin{bmatrix}
0 & (\mathbb{M}^{2,n})^{-1}\mathcal{T}^\top \mathbb{C}^\top & 0 & 0 & 0 \\
-\mathbb{C}\mathcal{T}(\mathbb{M}^{2,n})^{-1} & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0
\end{bmatrix}\,,
\\
\mathbb{J}_\text{CC-Denisty} &:= \begin{bmatrix}
-\frac{A_\textnormal{h}}{A_b} \frac{1}{\epsilon} \mathbb{L}²{\bar{B}}^\times_f \frac{1}{\bar g} \bar{W} \left(\bar 1- \frac{\bar B⁰_\parallel}{\bar B^{*0}_\parallel}\right) (\mathbb{L}²)^\top(\mathbb{M}^{2,n})^{-1} & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0
\end{bmatrix}\,,
\\
\mathbb{J}_\text{CC-GradB} &:= \begin{bmatrix}
0 & 0 & 0 & (\mathbb{M}^{2,n})^{-1} \mathbb{L}² \frac{1}{\bar{\sqrt{g}}} \frac{1}{\bar B^{*0}_\parallel}\bar{B}^\times_f \bar{G}^{-1} \bar{b}^\times_0 \bar{G}^{-1}  & 0 \\
0 & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 \\
-\bar{G}^{-1} \bar{b}^\times_0 \bar{G}^{-1}  \bar{B}^\times_f \frac{1}{\bar B^{*0}_\parallel} \frac{1}{\bar{\sqrt{g}}} (\mathbb{L}²)^\top (\mathbb{M}^{2,n})^{-1} & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0
\end{bmatrix}\,,
\\
\mathbb{J}_\text{CC-Curlb} &:= \begin{bmatrix}
0 & 0 & 0 & 0 & - (\mathbb{M}^{2,n})^{-1} \left\{\frac{1}{\bar{\sqrt{g}}}(\mathbb{L}²)^\top\right\}\cdot_\text{vector} \left\{\bar{b}^{\nabla \times}_0 (\bar{B}^\times_f)^\top \bar{V}_\parallel \frac{1}{\bar{\sqrt{g}}}\right\} \frac{1}{\bar B^{*0}_\parallel} \\ 
0 & 0 & 0 & 0 & 0 \\ 
0 & 0 & 0 & 0 & 0 \\ 
0 & 0 & 0 & 0 & 0 \\ 
\frac{1}{\bar B^{*0}_\parallel} \left\{\bar{b}^{\nabla \times}_0 (\bar{B}^\times_f)^\top \bar{V}_\parallel \frac{1}{\bar{\sqrt{g}}}\right\}\, \cdot_\text{vector} \left\{\frac{1}{\bar{\sqrt{g}}}(\mathbb{L}²)^\top\right\} (\mathbb{M}^{2,n})^{-1}& 0 & 0 & 0 & 0 
\end{bmatrix}\,,
\\
\mathbb{J}_\text{DK-GradB} &:= \begin{bmatrix}
0 & 0 & 0 & 0 & 0 \\ 
0 & 0 & 0 & 0 & 0 \\ 
0 & 0 & 0 & 0 & 0 \\ 
0 & 0 & 0 & \frac{A_b}{A_\textnormal{h}} (\bar{W})^{-1} \frac{1}{\bar B^{*0}_\parallel}\bar{G}^{-1}  \bar{b}^\times_0\bar{G}^{-1}  & 0 \\ 
0 & 0 & 0 & 0 & 0 
\end{bmatrix}\,,
\\
\mathbb{J}_\text{DK-Bstar} &:= \begin{bmatrix}
0 & 0 & 0 & 0 & 0 \\ 
0 & 0 & 0 & 0 & 0 \\ 
0 & 0 & 0 & 0 & 0 \\ 
0 & 0 & 0 & 0 & \frac{A_b}{A_\textnormal{h}} W^{-1} \frac{1}{\bar B^{*0}_\parallel} \left[\frac{1}{\bar{\sqrt{g}}} \bar{B}_f + \epsilon\bar{V}_\parallel \frac{1}{\bar{\sqrt{g}}}\bar{b}^{\nabla\times}_0 \right] \cdot_\text{scalar}  \\ 
0 & 0 & 0 & - \frac{A_b}{A_\textnormal{h}} W^{-1} \frac{1}{\bar B^{*0}_\parallel} \left[\frac{1}{\bar{\sqrt{g}}} \bar{B}_f + \epsilon\bar{V}_\parallel \frac{1}{\bar{\sqrt{g}}}\bar{b}^{\nabla\times}_0 \right] \cdot_\text{vector}  & 0
\end{bmatrix}\,.
\end{aligned}
\end{equation}
$$
In addition to 6 anti-symmetric sub-steps, we define one non-energy conserving sub-step which includes all the rest terms including the step for the Magnetosonic wave:
$$
\mathbb{K} = \mathbb{K}_\text{Magneto Sonic}
$$
where
$$
\begin{equation}
\mathbb{K}_\text{Magneto Sonic} := \begin{bmatrix}
0 & \mathbb{M}^{2,J} & \mathbb{D}^\top \mathbb{M}^3 & (\mathcal{T}^B)^\top \mathbb{C}^\top (\mathcal{M}\mathcal{C})^\top \boldsymbol{\eta}^{-1} & 0 \\
0 & 0 & 0 & 0 & 0 \\
-\mathcal{S}^\top \mathbb{D}^\top- (\gamma-1)\mathbb{D}^\top \mathcal{K}^\top & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 \\
\end{bmatrix}
\end{equation} \,.
$$
These sub-steps are called [Propagators](https://struphy.pages.mpcdf.de/struphy/sections/subsections/propagators.html#particle-propagators) in STRUPHY and these are maps $\Phi^n_{\Delta t}:\mathbf{Z}(t) \rightarrow \mathbf{Z}(t+\Delta t)$ which are the solution of
$$
\dot{\mathbf{Z}} = \mathbb{J}_n (\mathbf{Z}) \nabla \tilde{\mathcal{H}}_h (\mathbf{Z}) \,, \quad \text{or} \quad \dot{\mathbf{Z}} = \mathbb{K}(\mathbf{Z}) \mathbf{Z} \,.
$$
By composing each sub-steps successively, we can define a map of entire system in a single time step:
$$
\begin{equation}
\begin{aligned}
\Phi^L_{\Delta t} &:= \Phi^\text{DK-Bstar}_{\Delta t}\, \circ \Phi^\text{DK-GradB}_{\Delta t}\, \circ \Phi^\text{CC-Curlb}_{\Delta t}\, \circ \Phi^\text{CC-GradB}_{\Delta t}\, \circ \Phi^\text{CC-Density}_{\Delta t}\, \circ \Phi^\text{Magneto Sonic}_{\Delta t}\, \circ \Phi^\text{Shear Alfvén}_{\Delta t}\,,
\\
\Phi^S_{\Delta t} &:= \Phi^L_{\Delta t/2} \circ (\Phi^L_{\Delta t/2})^{-1} \,.
\end{aligned}
\end{equation}
$$
The first-order scheme $\Phi^L_{\Delta t}$ is called `Lie-Trotter` splitting and the second-order scheme $\Phi^S_{\Delta t}$ is called `Strang` splitting.

## Info

* Project executive(s): Byung Kyu Na
* Duration: ongoing
* Context: PhD thesis
* Funding: Max Planck IPP
* Start date: 07/2022
* End date: open
* Supervisor: Eric Sonnendrücker, [Stefan Possanner](https://www.linkedin.com/in/stefan-possanner-52a1a250/)

## Links

* [Cesare Tronci 2020](https://iopscience.iop.org/article/10.1088/1361-6587/ab7c4d/meta)
* [J. W. Burby 2016](https://iopscience.iop.org/article/10.1088/1361-6587/aa5c5b)

## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)

 
